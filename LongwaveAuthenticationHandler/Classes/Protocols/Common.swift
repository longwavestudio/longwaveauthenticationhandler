//
//  Common.swift
//  AuthenticationHandler
//
//  Created by Alessio Bonu on 26/11/2019.
//  Copyright © 2019 LongWave. All rights reserved.
//

import Foundation

public typealias onBackDidTap = () -> Void
