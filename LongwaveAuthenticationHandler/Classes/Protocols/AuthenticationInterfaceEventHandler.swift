//
//  RegistrationInterface.swift
//  AuthenticationHandler
//
//  Created by Alessio Bonu on 26/11/2019.
//  Copyright © 2019 LongWave. All rights reserved.
//

import UIKit
import LongwaveUtils

public enum FieldType {
    case name(text: String, textType: RegistrationTextFieldType, optional: Bool)
    case surname(text: String, textType: RegistrationTextFieldType, optional: Bool)
    case email(text: String, textType: RegistrationTextFieldType, optional: Bool)
    case password(text: String, textType: RegistrationTextFieldType, optional: Bool)
    case confirmPassword(text: String, textType: RegistrationTextFieldType, optional: Bool)
    case requestButton(text: String)
    case text(attributedtext: NSAttributedString)
    
    public func isSecured() -> Bool {
        switch self {
        case.confirmPassword(_, _, _),
            .password(_, _, _):
            return true
        case .name(_, _, _),
             .surname(_, _, _),
             .email(_, _, _),
             .requestButton(_),
             .text(_):
            return false
        }
    }
    
    public func isOptional() -> Bool {
        switch self {
        case .name(_, _, let optional),
             .surname(_, _, let optional),
             .email(_, _, let optional),
             .password(_, _, let optional),
             .confirmPassword(_, _, let optional):
            return optional
        case .requestButton(_),
             .text(_):
            return true
        }
    }
}

extension FieldType {
    func text() -> String? {
        switch self {
        case .name(let text, _, _),
             .surname(let text, _, _),
             .email(let text, _, _),
             .password(let text, _, _),
             .confirmPassword(let text, _, _):
            return text
        case .requestButton(_),
             .text(_):
            return nil
        }
    }
}

extension FieldType: Equatable {
    public static func == (lhs: FieldType, rhs: FieldType) -> Bool {
        switch (lhs, rhs) {
        case (.name(_, _, _), .name(_, _, _)),
             (.surname(_, _, _), .surname(_, _, _)),
             (.email(_, _, _), .email(_, _, _)),
             (.password(_, _, _), .password(_, _, _)),
             (.confirmPassword(_, _, _), .confirmPassword(_, _, _)),
             (.requestButton(_), .requestButton(_)),
             (.text(_), .text(_)):
            return true
        default:
            return false
        }
        
        
    }
}
public struct AuthenticationField {
    var type: FieldType
    var value: String
}

public typealias OnAuthenticationEventDidOccur = ([AuthenticationField]) -> Void

public protocol AuthenticationInterfaceEventHandler {
    var fields: [FieldType]? { get }
    var onBackDidTap: onBackDidTap? { get set }
    var onForgotPasswordDidTap: onBackDidTap? { get set }
    var onFormWillSend: OnAuthenticationEventDidOccur? { get set }
    var onHeaderRequested: (() -> UIView?)? { get set }
    var onError: ((Error) -> Void?)? { get set }
    
    func setup(fields: [FieldType],
               theme: AuthTheme,
               factory: AuthenticationViewControllerFactory)
    func setup(fields: [FieldType],
               theme: AuthTheme,
               fieldChecker: FieldChecker,
               factory: AuthenticationViewControllerFactory)
    func setup(fields: [FieldType],
               theme: AuthTheme,
               keyboardObserver: KeyboardObservable,
               factory: AuthenticationViewControllerFactory)
    func setup(fields: [FieldType],
               theme: AuthTheme,
               fieldChecker: FieldChecker,
               keyboardObserver: KeyboardObservable,
               factory: AuthenticationViewControllerFactory)
}

public extension AuthenticationInterfaceEventHandler {
    func setup(fields: [FieldType],
               theme: AuthTheme,
               factory: AuthenticationViewControllerFactory) {
        setup(fields: fields,
              theme: theme,
              fieldChecker: DefaultFieldChecker(),
              keyboardObserver: KeyboardObserver(),
              factory: factory)
    }
    
    func setup(fields: [FieldType],
               theme: AuthTheme,
               fieldChecker: FieldChecker,
               factory: AuthenticationViewControllerFactory) {
        setup(fields: fields,
              theme: theme,
              fieldChecker: fieldChecker,
              keyboardObserver: KeyboardObserver(),
              factory: factory)
    }
    
    func setup(fields: [FieldType],
               theme: AuthTheme,
               keyboardObserver: KeyboardObservable,
               factory: AuthenticationViewControllerFactory) {
        setup(fields: fields,
              theme: theme,
              fieldChecker: DefaultFieldChecker(),
              keyboardObserver: keyboardObserver,
              factory: factory)
    }
}
