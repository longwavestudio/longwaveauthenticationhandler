//
//  AuthenticationProvider.swift
//  AuthenticationHandler
//
//  Created by Alessio Bonu on 26/11/2019.
//  Copyright © 2019 LongWave. All rights reserved.
//

import Foundation
import LongwaveNetworkUtils

public struct Token {
    var token: String
    var refreshToken: String
    public init(token: String, refreshToken: String) {
        self.token = token
        self.refreshToken = refreshToken
    }
}

public typealias AuthenticationResponce = (Result<LoginResponse, Error>) -> Void
public typealias PasswordRecoveryResponce = (Result<Bool, Error>) -> Void

public protocol AuthenticationProvider {
    /// Authenticate with user and password
    /// - Parameters:
    ///   - user: the username to be used to login
    ///   - password: the password to be used to login
    ///   - completion: the completion block used to send the responce to the caller
    func login<T: LoginResponse>(username: String,
                                 password: String,
                                 completion: @escaping (Result<T, Error>) -> Void)
    
    func logout(completion: ((Result<VoidResponse, Error>) -> Void)?)
    
    /// Register the user
    /// - Parameters:
    ///   - info: the info to be sent to the server
    ///   - completion: the completion block used to send the responce to the caller
    func register<T: LoginResponse>(info: [AuthenticationField], completion: @escaping (Result<T, Error>) -> Void)
    
    /// Allows the user to recover the password
    /// - Parameters:
    ///   - email: the email address used by the user to register
    ///   - completion: the completion block used to send the responce to the caller
    func recoverPassword(email: String, completion: PasswordRecoveryResponce)
    
    /// Allows the app to reset the password
    /// - Parameters:
    ///   - otp: the otp to reset the password
    ///   - newPassword: the new password
    ///   - newPasswordCopy: the new password confirmed by the user
    ///   - completion: the completion block used to send the responce to the caller
    func passwordReset(otp:String, newPassword: String, newPasswordCopy: String, completion: @escaping AuthenticationResponce)
    
    /// Allows the app to refresh the current token
    /// - Parameters:
    ///   - completion: the completion block used to send the responce to the caller
    func refreshToken<T: LoginResponse>(completion: @escaping (Result<T, Error>) -> Void)
    
    /// Direct access to the accessToken If present
    func accessToken() -> String?
    
    /// Checks whether a user exists.
    /// - Parameters:
    ///   - email: the email or username of the user to be checked.
    ///   - completion: provides a successful result with a _true_ value if the user exists.
    func userExistsWithEmail(_ email: String, completion: @escaping (Result<Bool, Error>) -> Void)
}

