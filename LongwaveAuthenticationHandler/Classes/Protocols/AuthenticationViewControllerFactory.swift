//
//  AuthenticationViewControllerFactory.swift
//  AuthenticationHandler
//
//  Created by Alessio Bonu on 26/11/2019.
//  Copyright © 2019 LongWave. All rights reserved.
//

import UIKit
import LongwaveUtils

public protocol AuthenticationViewControllerFactory {
    /// Returns the viewController use to ask the user the credential to login with username and password
    func loginViewController(theme: AuthTheme) -> UIViewController & AuthenticationInterfaceEventHandler & ActivityHandler

    /// Returns the viewController used to register the user into  the system
    func registrationViewController(theme: AuthTheme) -> UIViewController & AuthenticationInterfaceEventHandler & ActivityHandler
     
    /// Returns the viewController used to recover the user password
    func passwordRecoveryViewController() -> UIViewController & AuthenticationInterfaceEventHandler
   
    /// Returns the viewController use to ask the user the credential to login with username and password
    func loginViewController(theme: AuthTheme, bundle: Bundle) -> UIViewController & AuthenticationInterfaceEventHandler & ActivityHandler

    /// Returns the viewController used to register the user into  the system
    func registrationViewController(theme: AuthTheme, bundle: Bundle) -> UIViewController & AuthenticationInterfaceEventHandler & ActivityHandler
    
    /// Returns the viewController used to recover the user password
    func passwordRecoveryViewController(bundle: Bundle) -> UIViewController & AuthenticationInterfaceEventHandler
    
    /// Shows an error in a popup
    /// - Parameters:
    ///   - title: the title of the popup
    ///   - message: the messa
    func errorViewController(title: String, message: String) -> UIViewController
}

extension AuthenticationViewControllerFactory {
    public func loginViewController(theme: AuthTheme) -> UIViewController & AuthenticationInterfaceEventHandler & ActivityHandler {
        loginViewController(theme: theme, bundle: Bundle.main)
    }

    public func registrationViewController(theme: AuthTheme) -> UIViewController & AuthenticationInterfaceEventHandler & ActivityHandler {
        registrationViewController(theme: theme, bundle: Bundle.main)
    }
    
    public func passwordRecoveryViewController() -> UIViewController & AuthenticationInterfaceEventHandler {
        passwordRecoveryViewController(bundle: Bundle.main)
    }
}
