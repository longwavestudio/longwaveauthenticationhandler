//
//  FieldChecker.swift
//  AuthenticationHandler
//
//  Created by Alessio Bonu on 09/01/2020.
//  Copyright © 2020 LongWave. All rights reserved.
//

import Foundation

public enum AuthenticationFieldType {
    case username
    case password
}

public enum FieldCheckResult: Error {
    case consistent
    case checkError(String)
}

public protocol FieldChecker {
    ///  Check the text and returns FieldCheckResult.consistent if it is good for the text field type, checkError with a string representing the error if not consistent
    /// - Parameters:
    ///   - text: the text to be checked
    ///   - type: the text field type
    func check(text: String, for type: AuthenticationFieldType) -> FieldCheckResult
}
