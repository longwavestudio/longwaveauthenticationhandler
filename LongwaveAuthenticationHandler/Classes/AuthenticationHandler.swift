//
//  AuthenticationHandler.swift
//  AuthenticationHandler
//
//  Created by Alessio Bonu on 26/11/2019.
//  Copyright © 2019 LongWave. All rights reserved.
//

import UIKit
import LongwaveUtils
import LongwaveNetworkUtils

enum LoginNetworkError: Error {
    case tokenNotFoundInSuccessResult
}

public enum AuthenticationViewType {
    case login
    case registration
    case forgotPassword
    case resetPassword
}

public struct AuthCellTheme {
    var placeHolderFont: UIFont
    var placeHolderColor: UIColor
    var textFont: UIFont
    var textColor: UIColor
    
    public init(placeHolderFont: UIFont,
                placeHolderColor: UIColor,
                textFont: UIFont,
                textColor: UIColor) {
        self.placeHolderFont = placeHolderFont
        self.placeHolderColor = placeHolderColor
        self.textFont = textFont
        self.textColor = textColor
    }
    
    public static func defaultAuthCellTheme() -> AuthCellTheme {
        return AuthCellTheme(placeHolderFont: UIFont.systemFont(ofSize: 18),
                             placeHolderColor: UIColor(red: 0.407,
                                                       green: 0.482,
                                                       blue: 0.575,
                                                       alpha: 1),
                             textFont: UIFont.boldSystemFont(ofSize: 20),
                             textColor: UIColor(red: 0.09,
                                                green: 0.196,
                                                blue: 0.302,
                                                alpha: 1))
    }
}

public struct ButtonCellTheme {
    var color: UIColor
    var textColor: UIColor
    var borderRadius: CGFloat
    
    public init( color: UIColor,
                 textColor: UIColor,
                 borderRadius: CGFloat) {
        self.color = color
        self.textColor = textColor
        self.borderRadius = borderRadius
    }
    
    public static func defaultButtonTheme() -> ButtonCellTheme {
        return ButtonCellTheme(color: UIColor(red: 0.18,
                                              green: 0.267,
                                              blue: 0.576,
                                              alpha: 1),
                               textColor: .white,
                               borderRadius: 4)
    }
}

public struct AuthTheme {
    var cellTheme: AuthCellTheme
    var buttonCellTheme: ButtonCellTheme
    var navigationBarStyle: NavigationBarStyle
    
    public init (cellTheme: AuthCellTheme,
                 buttonCellTheme: ButtonCellTheme,
                 navigationBarStyle: NavigationBarStyle) {
        self.cellTheme = cellTheme
        self.buttonCellTheme = buttonCellTheme
        self.navigationBarStyle = navigationBarStyle
    }
    
    public static func defaultTheme() -> AuthTheme {
        let barStyle = NavigationBarStyle.defaultNavigationBarStyle()
        let buttonStyle = ButtonCellTheme.defaultButtonTheme()
        let cellStyle = AuthCellTheme.defaultAuthCellTheme()
        return AuthTheme(cellTheme: cellStyle,
                         buttonCellTheme: buttonStyle,
                         navigationBarStyle: barStyle)
    }
}

public enum NavigationButtonType {
    case image(image: UIImage)
    case title(string: String)
    case none
}

public struct NavigationBarStyle {
    var navigationStyleCustomizationBlock: (UINavigationController) -> Void
    var buttonType: NavigationButtonType
    var title: String
    
    public init(buttonType: NavigationButtonType,
         title: String,
         navigationStyleCustomizationBlock: @escaping (UINavigationController) -> Void) {
        self.navigationStyleCustomizationBlock = navigationStyleCustomizationBlock
        self.buttonType = buttonType
        self.title = title
    }
    
    static func defaultNavigationBarStyle() -> NavigationBarStyle {
        return NavigationBarStyle(buttonType: .title(string: "Indietro".localizedString()),
                                  title: "") { _ in }
    }
}

public typealias OnAuthenticationDidComplete<T> = (Result<T?, Error>) -> Void
public typealias OnCustomHeaderRequested = (_ viewType: AuthenticationViewType) -> UIView?
public typealias OnThemeRequested = () -> AuthTheme

/// An authentication handler is a component that implements the whole authentication flow in terms of UI components (Login, Registration, Password recovery and refresh token).
public final class AuthenticationHandler {
    
    public var onCustomHeaderRequested: OnCustomHeaderRequested?
    public var onThemeRequested: OnThemeRequested = {
        return AuthTheme.defaultTheme()
    }
    
    private var factory: AuthenticationViewControllerFactory = DefaultAuthenticationViewControllerFactory()
    private var service: AuthenticationProvider?
    
    /// Creates an authentication handler
    /// - Parameters:
    ///   - viewControllerFactory: The factory to provide viewControllers
    ///   - service: the API service
    /// - Note: If the viewControllerFatctory is set to nil, a default Factory will be used
    /// - Note: If the service is set to nil, you should call setup before using it
    public init(viewControllerFactory: AuthenticationViewControllerFactory? = nil,
                service: AuthenticationProvider? = nil) {
        if let viewControllerFactory = viewControllerFactory {
            factory = viewControllerFactory
        }
        self.service = service
    }
    
    /// Setup the Handler adding the service
    /// - Parameter service: the service to be used
    public func setup(service: AuthenticationProvider) {
        self.service = service
    }
    
    /// Allows the caller to be authenticated in the system. Authentication can be done in different ways (login, registration or just a refresh token)
    /// - Parameters:
    ///   - presenter: The UIViewController used to present the Login Interface
    ///   - completion: The completion block called when the login request is completed
    public func authenticate<T: LoginResponse>(on presenter: UIViewController,
                             completion: @escaping (Result<T?, Error>) -> Void) {
        presentLogin(on: presenter, completion: completion)
    }
    
    /// Allows the caller to register in the system.
    /// - Parameters:
    ///   - presenter: The UIViewController used to present the Login Interface
    ///   - completion: The completion block called when the login request is completed
    public func register<T: LoginResponse>(on presenter: UIViewController,
                         completion: @escaping (Result<T?, Error>) -> Void) {
        presentRegistration(on: presenter, completion: completion)
    }
    
    /// Asks the server to refresh the token.
    /// - Parameters:
    ///   - token: the token to ben refreshed
    ///   - completion: the completion block called when the login request is completed
    public func refresh<T: LoginResponse>(completion: @escaping (Result<T, Error>) -> Void) {
        service?.refreshToken(completion: completion)
    }
    
    /// Asks the server to reset the password
    /// - Parameters:
    ///   - otp: the otp to reset the password
    ///   - completion: the completion block called when the login request is completed
    public func passwordReset<T: LoginResponse>(otp: String,
                              completion: @escaping (Result<T?, Error>) -> Void) {
        /// TODO: implemente password reset
    }
}

// Private methods
extension AuthenticationHandler {
    
    private func presentLogin<T: LoginResponse>(on presenter: UIViewController,
                              completion: @escaping (Result<T?, Error>) -> Void) {
        let currentBundle = Bundle(for: AuthenticationHandler.self)
        let theme = onThemeRequested()
        var loginViewController = factory.loginViewController(theme: theme, bundle: currentBundle)
        loginViewController.onBackDidTap = {
            completion(Result.success(nil))
        }
        loginViewController.onForgotPasswordDidTap = { [weak self, weak loginViewController] in
            self?.presentPasswordRecovery(on: loginViewController)
        }
        if let header = onCustomHeaderRequested {
            loginViewController.onHeaderRequested = {
                return header(.login)
            }
        }
        loginViewController.onError = { [weak self, weak loginViewController] error in
            self?.showError(error: error, errorPresenter: loginViewController)
        }
        loginViewController.onFormWillSend = { [weak self, weak loginViewController] info in
            let user = info[0].value
            let password = info[1].value
            loginViewController?.startActivity()
            let block: (Result<T, Error>) -> Void = { result in
                loginViewController?.stopActivity()
                self?.handleResult(result,
                                   errorPresenter: loginViewController) { result in
                                    switch result {
                                    case .success(let login):
                                        completion(.success(login))
                                    case .failure(let error):
                                        completion(.failure(error))
                                    }
                }
            }
            self?.service?.login(username: user,
                                password: password,
                                completion: block)
        }
        let navigationController = UINavigationController(rootViewController: loginViewController)
        navigationController.modalPresentationStyle = .fullScreen
        presenter.present(navigationController, animated: true, completion: nil)
    }
    
    private func presentRegistration<T: LoginResponse>(on presenter: UIViewController?, completion: @escaping (Result<T?, Error>) -> Void) {
        guard let presenter = presenter else { return }
        let currentBundle = Bundle(for: AuthenticationHandler.self)
        let theme = onThemeRequested()
        var registrationVC = self.factory.registrationViewController(theme: theme, bundle: currentBundle)
        registrationVC.onBackDidTap = {
            completion(Result.success(nil))
        }
        registrationVC.onError = { [weak self, weak registrationVC] error in
            self?.showError(error: error, errorPresenter: registrationVC)
        }
        
        registrationVC.onFormWillSend = { [weak self, weak registrationVC] info in
            let block: (Result<T, Error>) -> Void = { result in
                self?.handleResult(result, errorPresenter: registrationVC, completion: completion)
            }
            self?.service?.register(info: info, completion: block)
        }
        let navigationController = UINavigationController(rootViewController: registrationVC)
        navigationController.modalPresentationStyle = .fullScreen
        navigationController.setNavigationBarHidden(true, animated: false)
        presenter.present(navigationController, animated: true, completion: nil)
    }
    
    private func handleResult<T: LoginResponse>(_ result: Result<T, Error>,
                              errorPresenter presenter: UIViewController?,
                              completion: @escaping (Result<T?, Error>) -> Void) {
        switch result {
        case .success(let login):
            completion(.success(login))
        case .failure(let error):
            showError(error: error, errorPresenter: presenter)
        }
    }
    
    private func showError(error: Error, errorPresenter presenter: UIViewController?) {
        let title: String = "errorTitle".localizedString()
        let message: String
        switch error {
        case FieldTypeError.nonOptionalFieldNotSet:
            message = "missingParametersLogin".localizedString(in: Bundle.main)
        case let error as ApiError:
            if error.code == 210001 {
                message = "expiredSubscription".localizedString(in: Bundle.main)
            } else if error.code == 150006 {
                message = "subscriptionNotFound".localizedString(in: Bundle.main)
            } else {
                message = "userNameOrPasswordRequired".localizedString(in: Bundle.main)
            }
        default:
            message = "userNameOrPasswordRequired".localizedString(in: Bundle.main)
        }
        let alert = factory.errorViewController(title: title, message: message)
        presenter?.present(alert, animated: true, completion: nil)
    }
    
    private func presentPasswordRecovery(on presenter: UIViewController?) {
        guard let presenter = presenter else { return }
        var passwordRecovery = factory.passwordRecoveryViewController()
        passwordRecovery.onBackDidTap = { [weak passwordRecovery] in
            passwordRecovery?.dismiss(animated: true, completion: nil)
        }
        passwordRecovery.onFormWillSend = { [weak self, weak passwordRecovery] info in
            let email = info[0].value
            self?.service?.recoverPassword(email: email) { result in
                switch result {
                case .success(_):
//                    passwordRecovery?.passwordRecoveredCorrectly()
                    break
                case .failure(let error):
                    self?.showError(error: error, errorPresenter: passwordRecovery)
                }
            }
        }
        let navigationController = UINavigationController(rootViewController: passwordRecovery)
        presenter.present(navigationController, animated: true, completion: nil)
    }
}
