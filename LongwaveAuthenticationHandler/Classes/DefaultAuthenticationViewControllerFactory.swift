//
//  DefaultAuthenticationViewControllerFactory.swift
//  AuthenticationHandler
//
//  Created by Alessio Bonu on 26/11/2019.
//  Copyright © 2019 LongWave. All rights reserved.
//

import UIKit
import LongwaveUtils

public final class DefaultAuthenticationViewControllerFactory {
    let loader: StoryboardLoader.Type
    
    private let onAuthenticationViewController: () -> ViewControllerAuthenticable? = {
        let finder: DefaultAuthenticationStoryboardFinder = DefaultAuthenticationStoryboardFinder()
        let currentBundle = Bundle(for: DefaultAuthenticationViewControllerFactory.self)
        let table: DefaultAuthenticationTableViewController? =
            try? DefaultStoryboardLoader.viewController(with: AuthenticationViewController.reusableTable.rawValue,
                                       finder: finder,
                                       bundle: currentBundle)
        return table
    }
    
    public init(loader: StoryboardLoader.Type = DefaultStoryboardLoader.self) {
        self.loader = loader
    }
}

extension DefaultAuthenticationViewControllerFactory: AuthenticationViewControllerFactory {
    
    public func loginViewController(theme: AuthTheme, bundle: Bundle = Bundle.main) -> UIViewController & AuthenticationInterfaceEventHandler & ActivityHandler {
        var loginVC = getCommonViewController(bundle: bundle)
        let fields: [FieldType] =  [.email(text: "Email",
                                           textType: .defaultTextField,
                                           optional: false),
                                    .password(text: "Password",
                                              textType: .defaultTextField,
                                              optional: false),
                                    .requestButton(text: "Accedi".localizedString())]
        loginVC?.setup(fields: fields,
                       theme: theme,
                       factory: self)
        
        loginVC?.onHeaderRequested = {
            let screenWidth = UIScreen.main.bounds.width
            let view = UIView(frame: CGRect(origin: CGPoint.zero,
                                            size: CGSize(width: screenWidth,
                                                         height: 100)))
            view.backgroundColor = .green
            return view
        }
        loginVC?.modalPresentationStyle = .fullScreen
        return loginVC ?? DefaultCustomizableAuthenticationViewController()
    }
    
    public func registrationViewController(theme: AuthTheme, bundle: Bundle = Bundle.main) -> UIViewController & AuthenticationInterfaceEventHandler & ActivityHandler {
        var registrationVC = getCommonViewController(bundle: bundle)
        let fields: [FieldType] =  [.email(text: "Email",
                                           textType: .defaultTextField,
                                          optional: false),
                                   .password(text: "Password",
                                             textType: .defaultTextField,
                                             optional: false),
                                   .confirmPassword(text: "Confirm password",
                                                    textType: .defaultTextField,
                                                    optional: false),
                                   .requestButton(text: "Registrati".localizedString())]
        registrationVC?.setup(fields: fields,
                              theme: theme,
                              factory: self)
        registrationVC?.onHeaderRequested = {
            let screenWidth = UIScreen.main.bounds.width
            let view = UIView(frame: CGRect(origin: CGPoint.zero,
                                            size: CGSize(width: screenWidth,
                                                         height: 100)))
            view.backgroundColor = .red
            return view
        }
        registrationVC?.modalPresentationStyle = .fullScreen
        return registrationVC ?? DefaultCustomizableAuthenticationViewController()
    }
    
    private func getCommonViewController(bundle: Bundle = Bundle.main) -> (UIViewController & AuthenticationInterfaceEventHandler & ActivityHandler)? {
        let finder = DefaultAuthenticationStoryboardFinder()
        let viewController: DefaultCustomizableAuthenticationViewController? =
            try? loader.viewController(with: AuthenticationViewController.customizable.rawValue,
                                       finder: finder,
                                       bundle: bundle)
        viewController?.onAuthenticationViewController = onAuthenticationViewController
        return viewController
    }
    
    public func errorViewController(title: String, message: String) -> UIViewController {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
        return alert
    }
    
    public func passwordRecoveryViewController(bundle: Bundle = Bundle.main) -> UIViewController & AuthenticationInterfaceEventHandler {
        return DefaultCustomizableAuthenticationViewController()
    }
    
}
