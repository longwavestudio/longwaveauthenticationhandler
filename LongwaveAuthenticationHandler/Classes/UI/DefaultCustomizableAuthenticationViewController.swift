//
//  DefaultCustomizableAuthenticationViewController.swift
//  AuthenticationHandler
//
//  Created by Alessio Bonu on 26/02/2020.
//  Copyright © 2020 LongWave. All rights reserved.
//

import UIKit
import LongwaveUtils

enum FieldsConsistency {
    case consistent
    case wrongUsername
    case wrongPassword
    case wrongBoth
}

final class DefaultCustomizableAuthenticationViewController: UIViewController {
    var onBackDidTap: onBackDidTap?
    var onFormWillSend: OnAuthenticationEventDidOccur?
    var onForgotPasswordDidTap: onBackDidTap?
    var onHeaderRequested: (() -> UIView?)?
    var onError: ((Error) -> Void?)?
    
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView?
    
    var onAuthenticationViewController: (() -> ViewControllerAuthenticable?)?
    private(set) var fields: [FieldType]?
    private(set) var fieldChecker: FieldChecker?
    private(set) var keyboardObserver: KeyboardObservable?
    private(set) var factory: AuthenticationViewControllerFactory?
    private(set) var theme: AuthTheme?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        embedStandardTableViewController()
        applyNavigationStyle()
    }
    
    private func applyNavigationStyle()  {
        guard let theme = self.theme else { return }
        if let navController = navigationController {
            theme.navigationBarStyle.navigationStyleCustomizationBlock(navController)
        }
        switch theme.navigationBarStyle.buttonType {
        case .image(let image):
            navigationItem.leftBarButtonItem = UIBarButtonItem(image: image.withRenderingMode(.alwaysOriginal),
                                                               style: .done,
                                                               target: self,
                                                               action: #selector(back))
        case .title(let string):
            navigationItem.leftBarButtonItem = UIBarButtonItem(title: string,
                                                               style: .done,
                                                               target: self,
                                                               action: #selector(back))
        case .none:
            break
        }
        self.title = theme.navigationBarStyle.title
    }
    
    @objc func back() {
           onBackDidTap?()
    }
    
    func setup(fields: [FieldType],
               theme: AuthTheme,
               fieldChecker: FieldChecker = DefaultFieldChecker(),
               keyboardObserver: KeyboardObservable = KeyboardObserver(),
               factory: AuthenticationViewControllerFactory) {
        self.fields = fields
        self.fieldChecker = fieldChecker
        self.keyboardObserver = keyboardObserver
        self.factory = factory
        self.theme = theme
    }
}

/// Private methods
extension DefaultCustomizableAuthenticationViewController {
    
    private func embedStandardTableViewController() {
        guard let defaultAuthentication = onAuthenticationViewController?(),
            let fields = fields, let theme = self.theme else {
                return
        }
        embed(viewController: defaultAuthentication)
        defaultAuthentication.setup(fieldsList: fields,
                                    theme: theme,
                                    keyboardObserver: keyboardObserver ?? KeyboardObserver(),
                                    onButtonDidTap: onFormWillSend ?? { _ in })
        defaultAuthentication.onHeaderRequested = onHeaderRequested
        defaultAuthentication.onError = onError
        activityIndicator?.hidesWhenStopped = true
        activityIndicator?.bringSubviewToFront(defaultAuthentication.view)
       }
}

extension DefaultCustomizableAuthenticationViewController: AuthenticationInterfaceEventHandler {}

extension DefaultCustomizableAuthenticationViewController: ActivityHandler {}
