//
//  DefaultFieldChecker.swift
//  AuthenticationHandler
//
//  Created by Alessio Bonu on 09/01/2020.
//  Copyright © 2020 LongWave. All rights reserved.
//

import Foundation

final class DefaultFieldChecker {
    
}

extension DefaultFieldChecker: FieldChecker {
    func check(text: String, for type: AuthenticationFieldType) -> FieldCheckResult {
        guard !text.isEmpty else {
            return .checkError("Chould contain at least one character")
        }
        return .consistent
    }    
}
