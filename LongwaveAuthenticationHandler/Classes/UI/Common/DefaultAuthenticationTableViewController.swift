//
//  DefaultAuthenticationTableViewController.swift
//  AuthenticationHandler
//
//  Created by Alessio Bonu on 26/11/2019.
//  Copyright © 2019 LongWave. All rights reserved.
//

import UIKit
import LongwaveUtils

enum FieldTypeError: Error {
    case nonOptionalFieldNotSet
    case outOfBounds
}

enum ValueError: Error {
    case outOfBounds
}

final class DefaultAuthenticationTableViewController: UIViewController {
    
    var onBack: onBackDidTap?
    var onError: ((Error) -> Void?)?
    var onHeaderRequested: (() -> UIView?)?
    var onAuthenticationButtonDidTap: OnAuthenticationButtonDidTap?
    var currentScrollView: UIScrollView? { authenticationTableView }
    
    @IBOutlet weak var authenticationTableView: UITableView!

    private(set) var fieldsList: [FieldType] = [] {
        didSet {
            for _ in fieldsList {
                results.append("")
            }
        }
    }
    private(set) var results: [String] = []
    private(set) var theme: AuthTheme?

    private var keyboardObserver: KeyboardObservable?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Back".localizedString(), style: .done, target: self, action: #selector(back))
        authenticationTableView.keyboardDismissMode = .onDrag
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        authenticationTableView?.tableHeaderView = onHeaderRequested?()
    }
    
    func setup(fieldsList: [FieldType],
               theme: AuthTheme,
               keyboardObserver: KeyboardObservable,
               onButtonDidTap: @escaping OnAuthenticationButtonDidTap) {
        self.fieldsList = fieldsList
        self.onAuthenticationButtonDidTap = onButtonDidTap
        self.theme = theme
    }
    
    @objc func back() {
        onBack?()
    }
}

extension DefaultAuthenticationTableViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return fieldsList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let reuseId = cellReuseId(at: indexPath.row)
        let cell = tableView.dequeueReusableCell(withIdentifier: reuseId,
                                                 for: indexPath)
        let index = indexPath.row
        do {
            let currentValue = try value(at: index)
            let currentField = try field(at: index)
            (cell as? CustomizableCell)?.setup(with: currentField,
                                               theme: theme ?? AuthTheme.defaultTheme(),
                                               value: currentValue,
                                               errorMessage: nil)
            var authCell = (cell as? AuthEventHandler)
            authCell?.onSendInfoDidTap = { [weak self] in
                guard let strongSelf = self,
                    strongSelf.allFieldsAreSet() else {
                        self?.onError?(FieldTypeError.nonOptionalFieldNotSet)
                        return
                }
                strongSelf.onAuthenticationButtonDidTap?(strongSelf.resultsList())
            }
            var textFieldCell = (cell as? TextDidChangeObserver)
            textFieldCell?.onTextDidChange = { [weak self] text in
                guard  let type = (textFieldCell as? AuthenticationTextFieldCell)?.currentType,
                    let index = self?.fieldsList.firstIndex(where: { $0 == type }) else {
                        return
                }
                self?.results[index] = text
            }
        } catch {
            print("Cell customization for cell <\(cell)> failed \(error)")
        }
        return cell
    }
    
    private func allFieldsAreSet() -> Bool {
        guard fieldsList.count == results.count else {
            return false
        }
        for i in 0..<results.count {
            if !fieldsList[i].isOptional() && results[i].isEmpty {
                return false
            }
        }
        return true
    }
}


extension DefaultAuthenticationTableViewController {
    private func field(at index: Int) throws -> FieldType {
        guard index < fieldsList.count else {
            throw FieldTypeError.outOfBounds
        }
        return fieldsList[index]
    }
    
    private func value(at index: Int) throws -> String {
        guard index < results.count else {
            throw ValueError.outOfBounds
        }
        return results[index]
    }
    
    private func cellReuseId(at index: Int) -> String {
        guard let currentField = try? field(at: index) else {
            return AuthenticationButtonCell.reuseId
        }
        switch currentField {
        case .email(_, let textType, _),
             .name(_, let textType, _),
             .confirmPassword(_, let textType, _),
             .password(_, let textType, _),
             .surname(_, let textType, _):
            switch textType {
            case .defaultTextField:
                return AuthenticationTextFieldCell.reuseIdTextField
            case .withMessageText(_, _):
                return AuthenticationTextFieldCell.reuseIdTextFieldWithMessage
            }
        case .requestButton(_):
            return AuthenticationButtonCell.reuseId
        case .text:
            return AuthenticationAttributedTextCell.reuseId
        }
    }
    
    private func resultsList() -> [AuthenticationField] {
        var toRet: [AuthenticationField] = []
        guard fieldsList.count == results.count else {
            return toRet
        }
        for i in 0..<fieldsList.count {
            toRet.append(AuthenticationField(type: fieldsList[i], value: results[i]))
        }
        return toRet
    }
}
extension DefaultAuthenticationTableViewController: TableViewAuthentication {}

extension DefaultAuthenticationTableViewController: KeyboardAdjustable {}
