//
//  AuthenticationButtonCell.swift
//  AuthenticationHandler
//
//  Created by Alessio Bonu on 06/02/2020.
//  Copyright © 2020 LongWave. All rights reserved.
//

import UIKit
import LongwaveUtils
final class AuthenticationButtonCell: UITableViewCell {
    @IBOutlet weak var registerButton: UIButton!
    var onSendInfoDidTap: (() -> Void)?
    
    static let reuseId = "AuthenticationButtonCell"
    
    @IBAction func registerDidTap() {
        onSendInfoDidTap?()
    }
}

extension AuthenticationButtonCell: CustomizableCell {
    func setup(with type: FieldType, theme: AuthTheme, value: Any, errorMessage: String?) {
        guard shouldCustomizeCell(with: type) else { return }
        registerButton.setTitle(buttonText(type: type), for: .normal)
        registerButton.layer.cornerRadius = theme.buttonCellTheme.borderRadius
        registerButton.backgroundColor = theme.buttonCellTheme.color
        registerButton.titleLabel?.textColor = theme.buttonCellTheme.textColor
    }
    
    private func buttonText(type: FieldType) -> String {
        switch type {
        case .requestButton(let text):
            return text
        default:
            return ""
        }
    }
    
    private func shouldCustomizeCell(with type: FieldType) -> Bool {
        switch type {
        case .requestButton(_):
            return true
        default:
            return false
        }
    }
}

extension AuthenticationButtonCell: AuthEventHandler {}
