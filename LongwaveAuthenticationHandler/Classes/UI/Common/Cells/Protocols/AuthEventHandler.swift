//
//  AuthEventHandler.swift
//  AuthenticationHandler
//
//  Created by Alessio Bonu on 27/02/2020.
//  Copyright © 2020 LongWave. All rights reserved.
//

import Foundation

public protocol AuthEventHandler {
    var onSendInfoDidTap: (() -> Void)? { get set }
}

