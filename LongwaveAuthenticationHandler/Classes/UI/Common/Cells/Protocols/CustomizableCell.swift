//
//  CustomizableCell.swift
//  AuthenticationHandler
//
//  Created by Alessio Bonu on 24/02/2020.
//  Copyright © 2020 LongWave. All rights reserved.
//

import Foundation

protocol CustomizableCell {
    func setup(with type: FieldType, theme: AuthTheme, value: Any)
    func setup(with type: FieldType, theme: AuthTheme, value: Any, errorMessage: String?)
}

extension CustomizableCell {
    func setup(with type: FieldType, theme: AuthTheme,value: Any) {
        setup(with: type, theme: theme, value: value, errorMessage: nil)
    }
}
