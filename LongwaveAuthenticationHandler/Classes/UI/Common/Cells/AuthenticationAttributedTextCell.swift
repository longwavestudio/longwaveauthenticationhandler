//
//  AuthenticationAttributedTextCell.swift
//  AuthenticationHandler
//
//  Created by Alessio Bonu on 06/02/2020.
//  Copyright © 2020 LongWave. All rights reserved.
//

import UIKit

final class AuthenticationAttributedTextCell: UITableViewCell {
    @IBOutlet weak var attributedLabel: UILabel!
    
    static let reuseId = "AuthenticationAttributedTextCell"
    
    func addAttributes(_ attributes: NSAttributedString) {
        attributedLabel.attributedText = attributes
    }
}

extension AuthenticationAttributedTextCell: CustomizableCell {
    func setup(with type: FieldType, theme: AuthTheme, value: Any, errorMessage: String?) {
        guard shouldCustomizeCell(with: type) else { return }
        attributedLabel.attributedText = value as? NSAttributedString
    }
    
    private func shouldCustomizeCell(with type: FieldType) -> Bool {
        switch type {
        case .text:
            return true
        default:
            return false
        }
    }
}
