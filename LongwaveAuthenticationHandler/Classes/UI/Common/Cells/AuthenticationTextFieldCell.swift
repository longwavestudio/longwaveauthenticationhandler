//
//  AuthenticationTextFieldCell.swift
//  AuthenticationHandler
//
//  Created by Alessio Bonu on 06/02/2020.
//  Copyright © 2020 LongWave. All rights reserved.
//

import UIKit
import LongwaveUtils

public enum TextFieldMessageStyle {
    case error
    case information
}

public enum RegistrationTextFieldType {
    case withMessageText(message: String, priority: TextFieldMessageStyle)
    case defaultTextField
}

struct TextWithMessage {
    var text: String
    var message: String
}

final class AuthenticationTextFieldCell: UITableViewCell {
    
    static let reuseIdTextField = "AuthenticationTextFieldCell"
    static let reuseIdTextFieldWithMessage = "AuthenticationTextFieldCellWithMessage"

    var onTextDidChange: ((String) -> Void)?
    
    @IBOutlet weak var inputTextField: UITextField!
    @IBOutlet weak var messageLabel: UILabel?
    
    private(set) var currentType: FieldType?
}

extension AuthenticationTextFieldCell: CustomizableCell {
    func setup(with type: FieldType, theme: AuthTheme, value: Any, errorMessage: String?) {
        guard shouldCustomizeCell(with: type) else { return }
        currentType = type
        inputTextField.text = (value as? TextWithMessage)?.text
        inputTextField.delegate = self
        let cellTheme = theme.cellTheme
        let text = type.text() ?? ""
        let placeholder = NSMutableAttributedString(string: text)
        placeholder.addAttributes([NSAttributedString.Key.foregroundColor: cellTheme.placeHolderColor, NSAttributedString.Key.font: cellTheme.placeHolderFont
        ], range: NSRange(location: 0, length: text.count))
        inputTextField.attributedPlaceholder = placeholder
        inputTextField.textColor = cellTheme.textColor
        inputTextField.font = cellTheme.textFont
        inputTextField.isSecureTextEntry = type.isSecured()
    }
    
    private func shouldCustomizeCell(with type: FieldType) -> Bool {
        switch type {
        case .requestButton(_), .text(_):
            return false
        default:
            return true
        }
    }
}

extension AuthenticationTextFieldCell: TextDidChangeObserver {}

extension AuthenticationTextFieldCell: UITextFieldDelegate {
    func textField(_ textField: UITextField,
                          shouldChangeCharactersIn range: NSRange,
                          replacementString string: String) -> Bool {
        return updatedTextField(textField,
                                shouldChangeCharactersIn: range,
                                replacementString: string)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return false
    }

}

