//
//  DefaultAuthenticationStoryboardFinder.swift
//  AuthenticationHandler
//
//  Created by Alessio Bonu on 09/01/2020.
//  Copyright © 2020 LongWave. All rights reserved.
//

import Foundation
import LongwaveUtils

enum ViewControllerCreationError: Error {
    case notFound
}

enum AuthenticationViewController: String {
    case reusableTable = "DefaultAuthenticationViewController"
    case customizable = "DefaultCustomizebleAuthenticationViewController"
}

final class DefaultAuthenticationStoryboardFinder {
    let storyboardName: String = ""
}

extension DefaultAuthenticationStoryboardFinder: StoryboardFinder {
    
    func storyboardId(for viewControllerId: String) throws -> String {
        guard let vc = AuthenticationViewController(rawValue: viewControllerId) else {
            throw ViewControllerCreationError.notFound
        }
        switch vc {
        case .customizable:
            return "Registration"
        case .reusableTable:
            return "Authentication"
        }
    }
    
    func viewControllers(inStoryboard id: String) -> [String] {
        return [AuthenticationViewController.reusableTable.rawValue, AuthenticationViewController.customizable.rawValue]
    }
    
    
}
