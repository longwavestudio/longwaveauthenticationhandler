//
//  TableViewAuthentication.swift
//  AuthenticationHandler
//
//  Created by Alessio Bonu on 26/02/2020.
//  Copyright © 2020 LongWave. All rights reserved.
//

import UIKit
import LongwaveUtils

typealias OnAuthenticationButtonDidTap = ([AuthenticationField]) -> Void

typealias ViewControllerAuthenticable = UIViewController & TableViewAuthentication

protocol TableViewAuthentication: AnyObject {
    var fieldsList: [FieldType] { get }
    var onHeaderRequested: (() -> UIView?)? { get set }
    var onError: ((Error) -> Void?)? { get set }
    var onAuthenticationButtonDidTap: OnAuthenticationButtonDidTap? { get set }

    func setup(fieldsList: [FieldType],
               theme: AuthTheme,
               onButtonDidTap: @escaping OnAuthenticationButtonDidTap)
    
    func setup(fieldsList: [FieldType],
               theme: AuthTheme,
               keyboardObserver: KeyboardObservable,
               onButtonDidTap: @escaping OnAuthenticationButtonDidTap)
}

extension TableViewAuthentication {
    func setup(fieldsList: [FieldType],
               theme: AuthTheme,
               onButtonDidTap: @escaping OnAuthenticationButtonDidTap) {
        setup(fieldsList: fieldsList,
              theme: theme,
              keyboardObserver: KeyboardObserver(),
              onButtonDidTap: onButtonDidTap)
    }
}
