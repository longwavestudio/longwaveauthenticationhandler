//
//  AuthenticationHandler.h
//  AuthenticationHandler
//
//  Created by Alessio Bonu on 25/11/2019.
//  Copyright © 2019 LongWave. All rights reserved.
//

#import <Foundation/Foundation.h>

//! Project version number for AuthenticationHandler.
FOUNDATION_EXPORT double AuthenticationHandlerVersionNumber;

//! Project version string for AuthenticationHandler.
FOUNDATION_EXPORT const unsigned char AuthenticationHandlerVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <AuthenticationHandler/PublicHeader.h>
