//
//  LoginService+AuthenticationHandler.swift
//  AuthenticationHandler
//
//  Created by Alessio Bonu on 19/04/2020.
//  Copyright © 2020 LongWave. All rights reserved.
//

import Foundation
import LongwaveUtils
import LongwaveNetworkUtils

extension LoginService: AuthenticationProvider {
    
    public func userExistsWithEmail(_ email: String, completion: @escaping (Result<Bool, Error>) -> Void) {
        
    }
    
    public func register<T: LoginResponse>(info: [AuthenticationField], completion: @escaping (Result<T, Error>) -> Void) {
        
    }
    
    public func recoverPassword(email: String, completion: (Result<Bool, Error>) -> Void) {
        
    }
    
    public func passwordReset(otp: String, newPassword: String, newPasswordCopy: String, completion: @escaping AuthenticationResponce) {
        
    }
}
