//
//  DefaultAuthenticationViewControllerTests.swift
//  AuthenticationHandlerTests
//
//  Created by Alessio Bonu on 24/02/2020.
//  Copyright © 2020 LongWave. All rights reserved.
//

import XCTest
@testable import AuthenticationHandler
import LongwaveUtils

class DefaultAuthenticationTableViewControllerTests: XCTestCase {

    private func getSut() -> DefaultAuthenticationTableViewController {
        let finder = DefaultAuthenticationStoryboardFinder()
        let registerVc = AuthenticationViewController.reusableTable.rawValue
        let bundle = Bundle(for: DefaultAuthenticationTableViewController.self)
        let sut: DefaultAuthenticationTableViewController = try! DefaultStoryboardLoader.viewController(with: registerVc, finder: finder, bundle: bundle)
        return sut
    }
    
    func testNumberOfSectionShouldBe1() {
        let sut = getSut()
        XCTAssertEqual(sut.numberOfSections(in: UITableView()), 1)
    }
    
    func testNumberOfRowsShouldBeEqualToTheNumberOfTypesSet() {
        let sut = getSut()
        _ = sut.view
        sut.setup(fieldsList: [.name(text: "aaa", textType: .defaultTextField,  optional: true),.requestButton(text: "aaa")], theme: AuthTheme.defaultTheme()) { _ in }
        XCTAssertEqual(sut.tableView(UITableView(),
                                     numberOfRowsInSection: 0), 2)
    }
    
    func testNameFieldTypeWithDefaultTextShouldShowOnlyTextField() {
        registrationCellTextWithoutMessage(for: .name(text: "aaa",
                                                      textType: .defaultTextField,
                                                      optional: true))
    }
    
    func testEmailFieldTypeWithDefaultTextShouldShowOnlyTextField() {
        registrationCellTextWithoutMessage(for: .email(text: "aaa",
                                                       textType: .defaultTextField,
                                                       optional: true))
    }
    
    func testPasswordFieldTypeWithDefaultTextShouldShowOnlyTextField() {
        registrationCellTextWithoutMessage(for: .password(text: "aaa",
                                                          textType: .defaultTextField,
                                                          optional: true))
    }
    
    func testSurnameFieldTypeWithDefaultTextShouldShowOnlyTextField() {
        registrationCellTextWithoutMessage(for: .surname(text: "aaa",
                                                         textType: .defaultTextField,
                                                         optional: true))
    }

    
    func testNameFieldTypeWithTextAndLabelShouldShowOTextFieldAndLabel() {
        let textType: RegistrationTextFieldType = .withMessageText(message: "",
                                                                   priority: .error)
        registrationCellTextWithMessage(for: .name(text: "aaa",
                                                   textType: textType,
                                                   optional: true))
    }
    
    func testEmailFieldTypeWithTextAndLabelShouldShowOTextFieldAndLabel() {
        let textType: RegistrationTextFieldType = .withMessageText(message: "",
                                                                   priority: .error)
        registrationCellTextWithMessage(for: .email(text: "aaa",
                                                    textType: textType,
                                                    optional: true))
    }
    
    func testPasswordFieldTypeWithTextAndLabelShouldShowOTextFieldAndLabel() {
        let textType: RegistrationTextFieldType = .withMessageText(message: "",
                                                                   priority: .error)
        registrationCellTextWithMessage(for: .password(text: "aaa",
                                                       textType: textType,
                                                       optional: true))
    }
    
    func testSurnameFieldTypeWithTextAndLabelShouldShowOTextFieldAndLabel() {
        let textType: RegistrationTextFieldType = .withMessageText(message: "",
                                                                   priority: .error)
        registrationCellTextWithMessage(for: .surname(text: "aaa",
                                                      textType: textType,
                                                      optional: true))
    }
    
    private func registrationCellTextWithoutMessage(for type: FieldType) {
        let sut = getSut()
        _ = sut.view
        sut.setup(fieldsList: [type], theme: AuthTheme.defaultTheme()) { _ in }
        let cell = sut.tableView(sut.authenticationTableView,
                                 cellForRowAt: IndexPath(row: 0, section: 0))
        XCTAssertTrue(cell is AuthenticationTextFieldCell)
        let typedCell = cell as? AuthenticationTextFieldCell
        XCTAssertNotNil(typedCell?.inputTextField)
        XCTAssertNil(typedCell?.messageLabel)
    }
    
    private func registrationCellTextWithMessage(for type: FieldType) {
        let sut = getSut()
        _ = sut.view
        sut.setup(fieldsList: [type], theme: AuthTheme.defaultTheme()) { _ in }
        let cell = sut.tableView(sut.authenticationTableView,
                                 cellForRowAt: IndexPath(row: 0, section: 0))
        XCTAssertTrue(cell is AuthenticationTextFieldCell)
        let typedCell = cell as? AuthenticationTextFieldCell
        XCTAssertNotNil(typedCell?.inputTextField)
        XCTAssertNotNil(typedCell?.messageLabel)
    }
    
    func testIfButtonTypeFieldIsRequestedAuthenticationButtonCellIsRetrieved() {
        let sut = getSut()
        _ = sut.view
        sut.setup(fieldsList: [.requestButton(text: "aaa")], theme: AuthTheme.defaultTheme()) { _ in }
        let cell = sut.tableView(sut.authenticationTableView,
                                 cellForRowAt: IndexPath(row: 0, section: 0))
        XCTAssertTrue(cell is AuthenticationButtonCell)
    }
    
    func testIfIndexOutOfBoundsIsRequestedAuthenticationButtonCellIsRetrievedAnyways() {
        let sut = getSut()
        _ = sut.view
        sut.setup(fieldsList: [.requestButton(text: "")], theme: AuthTheme.defaultTheme()) { _ in }
        let cell = sut.tableView(sut.authenticationTableView,
                                 cellForRowAt: IndexPath(row: 100, section: 0))
        XCTAssertTrue(cell is AuthenticationButtonCell)
    }
    
    func testIfTextTypeFieldIsRequestedRegistrationAttributedTextCellIsRetrieved() {
        let sut = getSut()
        _ = sut.view
        sut.setup(fieldsList: [.text(attributedtext: NSAttributedString(string: "aaaa"))], theme: AuthTheme.defaultTheme()) { _ in }
        let cell = sut.tableView(sut.authenticationTableView,
                                 cellForRowAt: IndexPath(row: 0, section: 0))
        XCTAssertTrue(cell is AuthenticationAttributedTextCell)
    }
    
    func testResultsValuesAreCreatedAccordingToFieldListZeroElements() {
        let sut = getSut()
        _ = sut.view
        sut.setup(fieldsList: [], theme: AuthTheme.defaultTheme()) { _ in }
        XCTAssertEqual(sut.results.count, 0)
    }
    
    func testResultsValuesAreCreatedAccordingToFieldListOneElement() {
        let sut = getSut()
        _ = sut.view
        sut.setup(fieldsList: [.text(attributedtext: NSAttributedString(string: "aaa"))], theme: AuthTheme.defaultTheme()) { _ in }
        XCTAssertEqual(sut.results.count, 1)
    }
        
    func testResultsValuesAreCreatedAccordingToFieldListTwoElements() {
        let sut = getSut()
        _ = sut.view
        sut.setup(fieldsList: [.text(attributedtext: NSAttributedString(string: "aaa")), .text(attributedtext: NSAttributedString(string: "bbb"))], theme: AuthTheme.defaultTheme()) { _ in }
        XCTAssertEqual(sut.results.count, 2)
    }
    
    func testOnViewWillAppearHeaderCallbackIsCalled() {
        let sut = getSut()
        _ = sut.view
        var isRequested = false
        sut.onHeaderRequested = {
            isRequested = true
            return UIView()
        }
        sut.viewWillAppear(true)
        XCTAssertTrue(isRequested)
    }
    
    func testOnBackCallbackIsCalled() {
        let sut = getSut()
        var backDidRequest = false
        sut.onBack = {
            backDidRequest = true
        }
        sut.back()
        XCTAssertTrue(backDidRequest)
    }
    
    func testWritingInsideCellResultShouldBeChanged() {
        let sut = getSut()
        sut.setup(fieldsList: [.name(text: "Name",
                                     textType: .defaultTextField,
                                     optional: true)], theme: AuthTheme.defaultTheme()) { _ in }
        _ = sut.view
        let cell = sut.tableView(sut.authenticationTableView,
                                 cellForRowAt: IndexPath(row: 0, section: 0)) as! AuthenticationTextFieldCell
        let currentTextField = cell.inputTextField
        currentTextField?.text = "aaa"
        _ = cell.textField(currentTextField!,
                           shouldChangeCharactersIn: NSRangeFromString("aaa"),
                           replacementString: "bbb")
        XCTAssertEqual(sut.results[0], "bbbaaa")
    }
    
    func testWritingInsideCellResultShouldNotBeChangedIfTypeIsIncorrect() {
        let sut = getSut()
        sut.setup(fieldsList: [.name(text: "Name",
                                     textType: .defaultTextField,
                                     optional: true)], theme: AuthTheme.defaultTheme()) { _ in }
        _ = sut.view
        let cell = sut.tableView(sut.authenticationTableView,
                                 cellForRowAt: IndexPath(row: 0, section: 0)) as! AuthenticationTextFieldCell
        sut.setup(fieldsList: [.email(text: "Email",
                                     textType: .defaultTextField,
                                     optional: true)], theme: AuthTheme.defaultTheme()) { _ in }
        let currentTextField = cell.inputTextField
        currentTextField?.text = "aaa"
        _ = cell.textField(currentTextField!,
                           shouldChangeCharactersIn: NSRangeFromString("aaa"),
                           replacementString: "bbb")
        XCTAssertEqual(sut.results[0], "")
    }
    
    func testOnButtonClickResultsAreSentForValidation() {
        let sut = getSut()
        var registerDidCall = false
        sut.setup(fieldsList: [.name(text: "Name",
                                     textType: .defaultTextField,
                                     optional: true),
                               .requestButton(text: "aa")], theme: AuthTheme.defaultTheme()) { info in
                                registerDidCall = true
                                XCTAssertEqual(info[0].type, .name(text: "Name",
                                                                   textType: .defaultTextField,
                                                                   optional: true))
                                XCTAssertEqual(info[0].value, "bbbaaa")
        }
        _ = sut.view
        let cell = sut.tableView(sut.authenticationTableView,
                                 cellForRowAt: IndexPath(row: 0,
                                                         section: 0)) as! AuthenticationTextFieldCell
        let currentTextField = cell.inputTextField
        currentTextField?.text = "aaa"
        _ = cell.textField(currentTextField!,
                           shouldChangeCharactersIn: NSRangeFromString("aaa"),
                           replacementString: "bbb")
        let buttonCell = sut.tableView(sut.authenticationTableView,
                                       cellForRowAt: IndexPath(row: 1, section: 0)) as! AuthenticationButtonCell
        buttonCell.onSendInfoDidTap?()
        XCTAssertTrue(registerDidCall)
    }
    
    func testOnButtonClickVoidResultIsSentIfTypeListHasChanged() {
        let sut = getSut()
        sut.setup(fieldsList: [.name(text: "Name",
                                     textType: .defaultTextField,
                                     optional: true),
                               .requestButton(text: "qaaa")], theme: AuthTheme.defaultTheme()) { _ in }
        _ = sut.view
        let cell = sut.tableView(sut.authenticationTableView,
                                 cellForRowAt: IndexPath(row: 0, section: 0)) as! AuthenticationTextFieldCell
        let currentTextField = cell.inputTextField
        currentTextField?.text = "aaa"
        _ = cell.textField(currentTextField!,
                           shouldChangeCharactersIn: NSRangeFromString("aaa"),
                           replacementString: "bbb")
        let buttonCell = sut.tableView(sut.authenticationTableView,
                                       cellForRowAt: IndexPath(row: 1, section: 0)) as! AuthenticationButtonCell
        var registerDidCall = false
        sut.setup(fieldsList: [.name(text: "Name",
                                     textType: .defaultTextField,
                                     optional: true)], theme: AuthTheme.defaultTheme()) { info in
                                        registerDidCall = true
                                        XCTAssertTrue(info.isEmpty)
        }
        buttonCell.onSendInfoDidTap?()
        XCTAssertTrue(registerDidCall)
    }
    
    func testDesignetedScrollViewForKeyboardObservingIsTheTableView() {
        let sut = getSut()
        _ = sut.view
        XCTAssertEqual(sut.currentScrollView, sut.authenticationTableView)
    }
}
