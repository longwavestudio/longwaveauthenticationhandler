//
//  DefaultPasswordRecoveryViewControllerTests.swift
//  AuthenticationHandlerTests
//
//  Created by Alessio Bonu on 27/11/2019.
//  Copyright © 2019 LongWave. All rights reserved.
//

import XCTest
@testable import AuthenticationHandler
import LongwaveUtils

class DefaultCustomizableAuthenticationViewControllerTests: XCTestCase {
    
    private func getSutWithFactory(requestView: Bool = true) -> UIViewController & AuthenticationInterfaceEventHandler & ActivityHandler {
        let sut = DefaultAuthenticationViewControllerFactory()
        let currentBundle = Bundle(for: DefaultAuthenticationViewControllerFactory.self)
        let viewController = sut.registrationViewController(theme: AuthTheme.defaultTheme(), bundle: currentBundle)
        if requestView {
            _ = viewController.view
        }
        return viewController
    }
    
    private func getSutWithoutFactory(requestView: Bool = true) -> UIViewController & AuthenticationInterfaceEventHandler & ActivityHandler {
        let finder = DefaultAuthenticationStoryboardFinder()
        let currentBundle = Bundle(for: DefaultAuthenticationViewControllerFactory.self)
        let viewController: DefaultCustomizableAuthenticationViewController =
            try! DefaultStoryboardLoader.viewController(with: AuthenticationViewController.customizable.rawValue,
                                       finder: finder,
                                       bundle: currentBundle)
        if requestView {
            _ = viewController.view
        }
        return viewController
    }
    
    func testDefaultViewControllerShouldNotBeEmbeddedIfFieldsAreNotSet() {
        let sut = getSutWithoutFactory()
        let foundEmbeddedVC = sut.children.contains { $0 is DefaultAuthenticationTableViewController
        }
        XCTAssertFalse(foundEmbeddedVC)
    }
    
    func testDefaultViewControllerShouldBeEmbedded() {
        let sut = getSutWithFactory()
        let foundEmbeddedVC = sut.children.contains { $0 is DefaultAuthenticationTableViewController
        }
        XCTAssertTrue(foundEmbeddedVC)
    }
    
    func testActivityIndicatorIsCorrectlyAdded() {
        let sut = getSutWithFactory()
        XCTAssertNotNil(sut.activityIndicator)
    }
    
    func testActivityIndicatorIsSetOnHidesWhenStopped() {
        let sut = getSutWithFactory()
        XCTAssertTrue(sut.activityIndicator?.hidesWhenStopped ?? false)
    }
    
    func testIfNavigationBarIsPresentBackButtonIsSeOnTheLeft() {
        let sut = getSutWithFactory()
        XCTAssertNotNil(sut.navigationItem.leftBarButtonItem)
    }
    
    func testBackButtonIsConnectedToBackEvent() {
        var sut = getSutWithFactory()
        var backEventDidReceive = false
        sut.onBackDidTap = {
            backEventDidReceive = true
        }
        (sut as? DefaultCustomizableAuthenticationViewController)?.back()
        XCTAssertTrue(backEventDidReceive)
    }
    
    func testSetupShouldSetAllFields() {
        let sut = getSutWithFactory()
        let email: FieldType = .email(text: "Email",
                                      textType: .defaultTextField,
                                      optional: true)
        sut.setup(fields: [email], theme: AuthTheme.defaultTheme(),
                  factory: AuthenticationViewControllerFactoryMock())
        XCTAssertTrue(sut.fields?.count == 1)
        XCTAssertTrue(sut.fields?.first == email)
    }
    
    func testHeaderIsRequestedWhenShowingEmbeddeController() {
        var sut = getSutWithFactory(requestView: false)
        var headerDidRequest = false
        sut.onHeaderRequested = {
            headerDidRequest = true
            return UIView()
        }
        _ = sut.view
        let foundEmbeddedVC = sut.children.first { $0 is DefaultAuthenticationTableViewController }
        foundEmbeddedVC?.viewWillAppear(true)
        XCTAssertTrue(headerDidRequest)
    }
}
/// AuthenticationInterfaceEventHandler default implementation
extension DefaultCustomizableAuthenticationViewControllerTests {
    func testSetupWithFieldsAndFactoryShouldWork() {
        let sut = getSutWithFactory()
       
        sut.setup(fields: [], theme: AuthTheme.defaultTheme(),
                  factory: AuthenticationViewControllerFactoryMock())
        XCTAssertTrue(sut.fields?.count == 0)
    }
    
    func testSetupWithFieldsAndFieldCheckerAndFactoryShouldWork() {
        let sut = getSutWithFactory()
        sut.setup(fields: [], theme: AuthTheme.defaultTheme(),
                  fieldChecker: DefaultFieldChecker(),
                  factory: AuthenticationViewControllerFactoryMock())
        XCTAssertTrue(sut.fields?.count == 0)
    }
    
    func testSetupWithFieldsAndKeyboardObserverAndFactoryShouldWork() {
        let sut = getSutWithFactory()
        sut.setup(fields: [], theme: AuthTheme.defaultTheme(),
                  keyboardObserver: KeyboardObserver(),
                  factory: AuthenticationViewControllerFactoryMock())
        XCTAssertTrue(sut.fields?.count == 0)
    }
}
