//
//  RegistrationCellsTests.swift
//  AuthenticationHandlerTests
//
//  Created by Alessio Bonu on 06/02/2020.
//  Copyright © 2020 LongWave. All rights reserved.
//

import XCTest
@testable import AuthenticationHandler

class AuthenticationCellsTests: XCTestCase {

    func testTextFieldCellNoMessageReuseIdIsRegistrationTextFieldCell() {
        XCTAssertEqual(AuthenticationTextFieldCell.reuseIdTextField, "AuthenticationTextFieldCell")
    }

    func testTextFieldCellMessageReuseIdIsRegistrationTextFieldCellWithMessage() {
        XCTAssertEqual(AuthenticationTextFieldCell.reuseIdTextFieldWithMessage, "AuthenticationTextFieldCellWithMessage")
    }
    
    func testAttributedTextCellMessageReuseIdIsRegistrationAttributedTextCell() {
        XCTAssertEqual(AuthenticationAttributedTextCell.reuseId, "AuthenticationAttributedTextCell")
    }

    func testAttributedTextCellSettingAttributedTextShouldSucceed() {
        let sut = AuthenticationAttributedTextCell()
        let label = UILabel()
        sut.attributedLabel = label
        sut.addAttributes(NSAttributedString(string: "attributedString"))
        XCTAssertEqual(sut.attributedLabel.attributedText?.string, "attributedString")
    }
    
    func testRegisterButtonCellReuseIdIsAuthenticationButtonCell() {
         XCTAssertEqual(AuthenticationButtonCell.reuseId, "AuthenticationButtonCell")
    }
    
    func testRegisterButtonCellTouchedShouldCallCallback() {
        let sut = AuthenticationButtonCell()
        let button = UIButton()
        sut.registerButton = button
        var registerDidTap = false
        sut.onSendInfoDidTap = {
            registerDidTap = true
        }
        sut.registerDidTap()
        XCTAssertTrue(registerDidTap)
    }
    
    func testRegistrationCellButtonWithWrongTypeShouldNotCustomize() {
        let sut = AuthenticationButtonCell()
        let button = UIButton()
        sut.registerButton = button
        sut.setup(with: .email(text: "aaa",
                               textType: .defaultTextField,
                               optional: true),
                  theme: AuthTheme.defaultTheme(),
                  value: "Register")
        XCTAssertNotEqual(sut.registerButton.titleLabel?.text, "Register")
    }
    
    func testRegistrationCellButtonWithRightTypeShouldCustomize() {
        let sut = AuthenticationButtonCell()
        let button = UIButton()
        sut.registerButton = button
        sut.setup(with: .requestButton(text: "Register"), theme: AuthTheme.defaultTheme(), value: "Register")
        XCTAssertEqual(sut.registerButton.titleLabel?.text, "Register")
    }
    
    func testTextCellWithWrongTypeShouldNotCustomize() {
        let sut = AuthenticationAttributedTextCell()
        let label = UILabel()
        sut.attributedLabel = label
        sut.setup(with: .requestButton(text: "Register"), theme: AuthTheme.defaultTheme(), value: "Register")
        XCTAssertNotEqual(sut.attributedLabel.attributedText?.string, "Register")
    }
    
    func testTextCellWithRightTypeShouldCustomize() {
        let sut = AuthenticationAttributedTextCell()
        let label = UILabel()
        sut.attributedLabel = label
        sut.setup(with: .text(attributedtext: NSAttributedString(string: "bbbb")), theme: AuthTheme.defaultTheme(), value: NSAttributedString(string: "aaaa"))
        XCTAssertNotEqual(sut.attributedLabel.attributedText, NSAttributedString(string: "bbbb"))
    }
    
    func testTextFieldCellWithWrongTypeShouldNotCustomize() {
        let sut = AuthenticationTextFieldCell()
        let textField = UITextField()
        sut.inputTextField = textField
        sut.setup(with: .requestButton(text: "Register"), theme: AuthTheme.defaultTheme(), value: "aaaa")
        XCTAssertNotEqual(sut.inputTextField.text, "aaaa")
    }
    
    func testTextFieldCellWithRightTypeShouldCustomize() {
        let sut = AuthenticationTextFieldCell()
        let textField = UITextField()
        sut.inputTextField = textField
        sut.setup(with: .name(text: "jj", textType: .defaultTextField, optional: true), theme: AuthTheme.defaultTheme(), value: TextWithMessage(text: "aaaa", message: "nbbb"))
        XCTAssertEqual(sut.inputTextField.text, "aaaa")
    }
    
    func testTextFieldCellPlaceholderIsTakenFromTextInsideType() {
        let sut = AuthenticationTextFieldCell()
        let textField = UITextField()
        sut.inputTextField = textField
        sut.setup(with: .name(text: "jj", textType: .defaultTextField, optional: true), theme: AuthTheme.defaultTheme(), value: "lll")
        XCTAssertEqual(sut.inputTextField.placeholder, "jj")
    }
    
    func testFieldTypeTextForButtonCellIsNil() {
        let sut = FieldType.requestButton(text: "aa")
        XCTAssertNil(sut.text())
    }
    
    func testFieldTypeTextForAttributedTextCellIsNil() {
        let sut = FieldType.text(attributedtext: NSAttributedString(string: "aaa"))
        XCTAssertNil(sut.text())
    }
}


