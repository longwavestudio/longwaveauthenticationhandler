//
//  DefaultAuthenticationViewControllerFactoryTests.swift
//  AuthenticationHandlerTests
//
//  Created by Alessio Bonu on 26/11/2019.
//  Copyright © 2019 LongWave. All rights reserved.
//

import XCTest
@testable import AuthenticationHandler
import LongwaveUtils

class DefaultAuthenticationViewControllerFactoryTests: XCTestCase {

    func testFactoryShouldReturnDefaultLogin() {
        let sut = DefaultAuthenticationViewControllerFactory()
        let currentBundle = Bundle(for: DefaultCustomizableAuthenticationViewController.self)
        XCTAssertTrue(sut.loginViewController(theme: AuthTheme.defaultTheme(), bundle: currentBundle) is DefaultCustomizableAuthenticationViewController)
    }
    
    func testFactoryShouldReturnDefaultRegistration() {
        let sut = DefaultAuthenticationViewControllerFactory()
        let currentBundle = Bundle(for: DefaultCustomizableAuthenticationViewController.self)
        XCTAssertTrue(sut.registrationViewController(theme: AuthTheme.defaultTheme(), bundle: currentBundle) is DefaultCustomizableAuthenticationViewController)
    }

    func testFactoryShowldReturnDefaultAlertForError() {
        let sut = DefaultAuthenticationViewControllerFactory()
        let expectedTitle = "aaa"
        let expectedMessage = "bbb"
        let alert = sut.errorViewController(title: expectedTitle, message: expectedMessage)
        XCTAssertTrue(alert is UIAlertController)
        XCTAssertEqual((alert as? UIAlertController)?.title, expectedTitle)
        XCTAssertEqual((alert as? UIAlertController)?.message, expectedMessage)
    }
    
    func testFactoryShouldReturnPasswordRecoveryViewControllerOnPasswordRecoveryButtonDidTap() {
        let sut = DefaultAuthenticationViewControllerFactory()
        XCTAssertTrue(sut.passwordRecoveryViewController() is DefaultCustomizableAuthenticationViewController)
    }
    
    func testLoginViewControllerReturnsNotSetViewControllerIfLoaderThrowsError() {
        StoryboardLoaderMock.result = .error(StoryboardLoaderTestError.testError)
        let sut = DefaultAuthenticationViewControllerFactory(loader: StoryboardLoaderMock.self)
        let loginVC = sut.loginViewController(theme: AuthTheme.defaultTheme()) as? DefaultCustomizableAuthenticationViewController
        XCTAssertNil(loginVC?.fieldChecker)
    }
    
    func testRegistrationViewControllerReturnsNotSetViewControllerIfLoaderThrowsError() {
        StoryboardLoaderMock.result = .error(StoryboardLoaderTestError.testError)
        let sut = DefaultAuthenticationViewControllerFactory(loader: StoryboardLoaderMock.self)
        let registrationVC = sut.registrationViewController(theme: AuthTheme.defaultTheme()) as? DefaultCustomizableAuthenticationViewController
        XCTAssertNotNil(registrationVC)
    }
    
    func testStoryboardIdForNotKnownIdShouldThrowViewControllerCreationError_NotFound() {
        let sut = DefaultAuthenticationStoryboardFinder()
        XCTAssertThrowsError(try sut.storyboardId(for: "aaaa"))
    }
    
    func testIfNotSpecifiedLoginViewControllerIsTakenFromTheMainBundle() {
        let sut = TestFactory()
        let _ = sut.loginViewController(theme: AuthTheme.defaultTheme())
        XCTAssertTrue(sut.isCalled)
    }
    
    func testIfNotSpecifiedRegistrationViewControllerIsTakenFromTheMainBundle() {
        let sut = TestFactory()
        let _ = sut.registrationViewController(theme: AuthTheme.defaultTheme())
        XCTAssertTrue(sut.isCalled)
    }
    
    func testHeaderViewForLoginViewcontrollerIsGreen() {
        let sut = DefaultAuthenticationViewControllerFactory(loader: DefaultStoryboardLoader.self)
        let loginVC = sut.loginViewController(theme: AuthTheme.defaultTheme(), bundle: Bundle(for: DefaultAuthenticationViewControllerFactory.self))
        let header = loginVC.onHeaderRequested?()
        XCTAssertEqual(header!.backgroundColor, UIColor.green)
    }
    
    func testHeaderViewForRegistrationViewcontrollerIsRed() {
        let sut = DefaultAuthenticationViewControllerFactory(loader: DefaultStoryboardLoader.self)
        let loginVC = sut.registrationViewController(theme: AuthTheme.defaultTheme(), bundle: Bundle(for: DefaultAuthenticationViewControllerFactory.self))
        let header = loginVC.onHeaderRequested?()
        XCTAssertEqual(header!.backgroundColor, UIColor.red)
    }
}


private class TestFactory: AuthenticationViewControllerFactory {
    func defaultAuthenticationTableViewController(bundle: Bundle) -> UIViewController {
        return UIViewController()
    }
    
    func passwordRecoveryViewController(bundle: Bundle) -> UIViewController & AuthenticationInterfaceEventHandler {
        return DefaultCustomizableAuthenticationViewController()
    }
    
    private(set) var isCalled = false
    func registrationViewController(theme: AuthTheme, bundle: Bundle) -> UIViewController & AuthenticationInterfaceEventHandler & ActivityHandler {
        isCalled = true
        return DefaultCustomizableAuthenticationViewController()
    }
    
    func errorViewController(title: String, message: String) -> UIViewController {
        return UIViewController()
    }
    
    func loginViewController(theme: AuthTheme, bundle: Bundle) -> UIViewController & AuthenticationInterfaceEventHandler & ActivityHandler{
        isCalled = true
        XCTAssertEqual(bundle, Bundle.main)
        return DefaultCustomizableAuthenticationViewController()
    }
}
