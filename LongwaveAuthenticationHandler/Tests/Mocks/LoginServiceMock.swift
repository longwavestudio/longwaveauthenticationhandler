//
//  LoginServiceMock.swift
//  AuthenticationHandlerTests
//
//  Created by Alessio Bonu on 26/11/2019.
//  Copyright © 2019 LongWave. All rights reserved.
//

import Foundation
@testable import AuthenticationHandler
import LongwaveNetworkUtils

enum LoginTestError: Error {
    case testError
}

final class LoginServiceMock {
    var result: Result<LoginResponseMock, Error> = .failure(LoginTestError.testError)
    var passwordRecoveryResult: Result<Bool, Error> = .failure(LoginTestError.testError)

    var onLoginRequestDidCall: ((_ user: String, _ psw: String) -> Void)?
    var onLogoutRequestDidCall: (() -> Void)?

    var onRegistrationRequestDidCall: ((_ info: [AuthenticationField]) -> Void)?
    var onPasswordRecoveryRequestDidCall: ((_ email: String) -> Void)?
    var onPasswordResetRequestDidCall: ((_ otp: String, _ newPassword: String,_ newPasswordConfirmation: String) -> Void)?

    var onTokenRefreshRequestDidCall: (() -> Void)?
}

extension LoginServiceMock: AuthenticationProvider {
    func userExistsWithEmail(_ email: String, completion: @escaping (Result<Bool, Error>) -> Void) {
        
    }
    
    func login<T: LoginResponse>(username: String, password: String, completion: @escaping (Result<T, Error>) -> Void)  {
        onLoginRequestDidCall?(username, password)
        completion(result as! Result<T, Error>)
    }
    
    func logout(completion: ((Result<VoidResponse, Error>) -> Void)?) {
        onLogoutRequestDidCall?()
    }
    
    func refreshToken<T: LoginResponse>(completion: @escaping (Result<T, Error>) -> Void)  {
        onTokenRefreshRequestDidCall?()
        completion(result as! Result<T, Error>)
    }
    
    func passwordReset(otp: String, newPassword: String, newPasswordCopy: String, completion: @escaping AuthenticationResponce) {
        onPasswordResetRequestDidCall?(otp, newPassword, newPasswordCopy)
    }
    
    func register<T: LoginResponse>(info: [AuthenticationField], completion: @escaping (Result<T, Error>) -> Void) {
        onRegistrationRequestDidCall?(info)
        completion(result as! Result<T, Error>)
    }
    
    func recoverPassword(email: String,
                         completion: PasswordRecoveryResponce) {
        onPasswordRecoveryRequestDidCall?(email)
        completion(passwordRecoveryResult)
    }
    
    func refreshToken(token: Token,
                      completion: @escaping (AuthenticationResponce)) {
        onTokenRefreshRequestDidCall?()
        completion(result as! Result<LoginResponse, Error>)
    }
    
    func accessToken() -> String? {
        return nil
    }
}
