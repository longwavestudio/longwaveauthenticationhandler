//
//  PasswordRecoveryViewcontrollerMock.swift
//  AuthenticationHandlerTests
//
//  Created by Alessio Bonu on 27/11/2019.
//  Copyright © 2019 LongWave. All rights reserved.
//

import UIKit
@testable import AuthenticationHandler
import LongwaveUtils

final class PasswordRecoveryViewcontrollerMock: PresenterMock {
    
    var onBackDidTap: onBackDidTap?
    var fields: [FieldType]?
    var onForgotPasswordDidTap: onBackDidTap? = nil
    var onFormWillSend: OnAuthenticationEventDidOccur?
    var onHeaderRequested: (() -> UIView?)?
    var onError: ((Error) -> Void?)?
    func setup(fields: [FieldType],
               theme: AuthTheme,
               fieldChecker: FieldChecker,
               keyboardObserver: KeyboardObservable,
               factory: AuthenticationViewControllerFactory) {
        
    }
}

extension PasswordRecoveryViewcontrollerMock: AuthenticationInterfaceEventHandler {}
