//
//  LoginResponseMock.swift
//  AuthenticationHandler
//
//  Created by Alessio Bonu on 19/04/2020.
//  Copyright © 2020 LongWave. All rights reserved.
//

import Foundation
import LongwaveNetworkUtils

struct LoginResponseMock: LoginResponse {
    func tokens() -> (accessToken: String, refreshToken: String)? {
        return (accessToken: "", refreshToken: "")
    }
}
