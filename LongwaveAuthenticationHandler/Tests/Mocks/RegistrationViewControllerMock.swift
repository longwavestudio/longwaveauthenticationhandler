//
//  RegistrationViewControllerMock.swift
//  AuthenticationHandlerTests
//
//  Created by Alessio Bonu on 26/11/2019.
//  Copyright © 2019 LongWave. All rights reserved.
//

import UIKit
@testable import AuthenticationHandler
import LongwaveUtils
final class RegistrationViewControllerMock: PresenterMock {
    var fields: [FieldType]?
    var onBackDidTap: onBackDidTap?
    var onError: ((Error) -> Void?)?
    var onFormWillSend: OnAuthenticationEventDidOccur?
    var onHeaderRequested: (() -> UIView?)?
    var onForgotPasswordDidTap: onBackDidTap?
    var activityIndicator: UIActivityIndicatorView?
    
}

extension RegistrationViewControllerMock: AuthenticationInterfaceEventHandler {
     
    func setup(fields: [FieldType],
               theme: AuthTheme,
               fieldChecker: FieldChecker,
               keyboardObserver: KeyboardObservable,
               factory: AuthenticationViewControllerFactory) {
        
    }
    
}

extension RegistrationViewControllerMock: ActivityHandler {}
