//
//  PresenterMock.swift
//  AuthenticationHandlerTests
//
//  Created by Alessio Bonu on 26/11/2019.
//  Copyright © 2019 LongWave. All rights reserved.
//

import UIKit

class PresenterMock: UIViewController {
    var onPresent: ((UIViewController) -> Void)?
    var onDismiss: (() -> Void)?

    var currentNavigation: UINavigationController?
    
    
    override func present(_ viewControllerToPresent: UIViewController, animated flag: Bool, completion: (() -> Void)? = nil) {
        if let vc = viewControllerToPresent as? UINavigationController {
            currentNavigation = vc
        }
        onPresent?(viewControllerToPresent)
        
    }
    
    override func dismiss(animated flag: Bool, completion: (() -> Void)? = nil) {
        onDismiss?()
    }
}
