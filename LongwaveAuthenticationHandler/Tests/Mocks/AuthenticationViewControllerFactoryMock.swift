//
//  AuthenticationViewControllerFactoryMock.swift
//  AuthenticationHandlerTests
//
//  Created by Alessio Bonu on 26/11/2019.
//  Copyright © 2019 LongWave. All rights reserved.
//

import UIKit
@testable import AuthenticationHandler
import LongwaveUtils

typealias OnLoginViewcontroller = (() -> UIViewController & AuthenticationInterfaceEventHandler & ActivityHandler)
typealias OnRegistrationViewcontroller = (() -> UIViewController & AuthenticationInterfaceEventHandler & ActivityHandler)
typealias OnDefaultAuthenticationTableViewController = (() -> UIViewController)
typealias OnErrorViewcontroller = (() -> UIViewController)

typealias OnPasswordRecoveryViewcontroller = (() -> UIViewController & AuthenticationInterfaceEventHandler)

final class AuthenticationViewControllerFactoryMock {
    var onLoginViewControllerRequested: OnLoginViewcontroller?
    var onRegistrationViewControllerRequested: OnRegistrationViewcontroller?
    var onErrorViewControllerRequested: OnErrorViewcontroller?
    var onPasswordRecoveryViewControllerRequested: OnPasswordRecoveryViewcontroller?
    var onDefaultAuthenticationTableViewController: OnDefaultAuthenticationTableViewController?
    
    init(onLoginViewControllerRequested: OnLoginViewcontroller? = nil,
         onRegistrationViewControllerRequested: OnRegistrationViewcontroller? = nil,
         onErrorViewControllerRequested: OnErrorViewcontroller? = nil,
         onPasswordRecoveryViewControllerRequested: OnPasswordRecoveryViewcontroller? = nil,
         onDefaultAuthenticationTableViewController: OnDefaultAuthenticationTableViewController? = nil) {
        self.onLoginViewControllerRequested = onLoginViewControllerRequested
        self.onRegistrationViewControllerRequested = onRegistrationViewControllerRequested
        self.onErrorViewControllerRequested = onErrorViewControllerRequested
        self.onPasswordRecoveryViewControllerRequested = onPasswordRecoveryViewControllerRequested
        self.onDefaultAuthenticationTableViewController = onDefaultAuthenticationTableViewController
    }
}

extension AuthenticationViewControllerFactoryMock: AuthenticationViewControllerFactory {
    func defaultAuthenticationTableViewController(bundle: Bundle) -> UIViewController {
        return onDefaultAuthenticationTableViewController?() ?? UIViewController()
    }
    
    func registrationViewController(theme: AuthTheme, bundle: Bundle) -> UIViewController & AuthenticationInterfaceEventHandler & ActivityHandler {
        return onRegistrationViewControllerRequested?() ?? DefaultCustomizableAuthenticationViewController()
    }
    
    func loginViewController(theme: AuthTheme, bundle: Bundle) -> UIViewController & AuthenticationInterfaceEventHandler & ActivityHandler {
        return onLoginViewControllerRequested?() ?? DefaultCustomizableAuthenticationViewController()
    }
    
    func errorViewController(title: String, message: String) -> UIViewController {
        return onErrorViewControllerRequested?() ?? UIAlertController()
    }
    
    func passwordRecoveryViewController(bundle: Bundle) -> UIViewController & AuthenticationInterfaceEventHandler {
        return onPasswordRecoveryViewControllerRequested?() ?? DefaultCustomizableAuthenticationViewController()
    }

}
