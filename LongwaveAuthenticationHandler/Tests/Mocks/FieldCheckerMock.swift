//
//  FieldCheckerMock.swift
//  AuthenticationHandlerTests
//
//  Created by Alessio Bonu on 09/01/2020.
//  Copyright © 2020 LongWave. All rights reserved.
//

import Foundation

import AuthenticationHandler

final class FieldCheckerMock {
    var resultUsername = FieldCheckResult.consistent
    var resultPassword = FieldCheckResult.consistent

    var onCheckExecuted: ((_ type: AuthenticationFieldType) -> Void)?
}

extension FieldCheckerMock: FieldChecker {
    func check(text: String, for type: AuthenticationFieldType) -> FieldCheckResult {
        onCheckExecuted?(type)
        switch type {
        case .password:
            return resultPassword
        case .username:
            return resultUsername
        }
    }
}
