//
//  StoryboardLoaderMock.swift
//  AuthenticationHandlerTests
//
//  Created by Alessio Bonu on 06/02/2020.
//  Copyright © 2020 LongWave. All rights reserved.
//

import UIKit
import AuthenticationHandler
import LongwaveUtils

enum StoryboardLoaderTestError: Error {
    case notConfigured
    case testError
}

enum StoryboardLoaderResult {
    case error(Error)
    case success(UIViewController)
}

final class StoryboardLoaderMock: StoryboardLoader {
    static var result: StoryboardLoaderResult?
    
    static func viewController<ViewController>(with id: String, finder: StoryboardFinder, bundle: Bundle?) throws -> ViewController {
        guard let result = result else {
            throw StoryboardLoaderTestError.notConfigured
        }
        switch result {
        case .error(let error):
            throw error
        case .success(let controller):
            guard  let viewController = controller as? ViewController else {
                throw StoryboardLoaderTestError.notConfigured
            }
            return viewController
        }
    }
    
    
}
