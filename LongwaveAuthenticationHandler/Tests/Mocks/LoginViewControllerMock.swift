//
//  LoginViewControllerMock.swift
//  AuthenticationHandlerTests
//
//  Created by Alessio Bonu on 26/11/2019.
//  Copyright © 2019 LongWave. All rights reserved.
//

import UIKit
@testable import AuthenticationHandler
import LongwaveUtils

final class LoginViewControllerMock: PresenterMock {
    var user: String = "Name"
    var password: String = "Password"
    var onBackDidTap: onBackDidTap?
    var fields: [FieldType]?
    var onHeaderRequested: (() -> UIView?)?
    var onFormWillSend: OnAuthenticationEventDidOccur?
    var onForgotPasswordDidTap: onBackDidTap?
    var activityIndicator: UIActivityIndicatorView?
    
    var onActivityDidStart: (() -> Void)?
    var onActivityDidStop: (() -> Void)?
    var onError: ((Error) -> Void?)?
}

extension LoginViewControllerMock: AuthenticationInterfaceEventHandler {
    
    func setup(fields: [FieldType], theme: AuthTheme, fieldChecker: FieldChecker, keyboardObserver: KeyboardObservable, factory: AuthenticationViewControllerFactory) {
        
    }
}

extension LoginViewControllerMock: ActivityHandler {
    func startActivity() {
        onActivityDidStart?()
    }
    
    func stopActivity() {
        onActivityDidStop?()
    }
}
