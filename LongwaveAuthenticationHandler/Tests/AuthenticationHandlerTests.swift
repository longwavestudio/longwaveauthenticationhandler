//
//  AuthenticationHandlerTests.swift
//  AuthenticationHandlerTests
//
//  Created by Alessio Bonu on 25/11/2019.
//  Copyright © 2019 LongWave. All rights reserved.
//

import XCTest
@testable import AuthenticationHandler
import LongwaveNetworkUtils

class AuthenticationHandlerTests: XCTestCase {
    typealias SutResponce = (authHandler: AuthenticationHandler, loginVc: LoginViewControllerMock, registrationVC: RegistrationViewControllerMock, passwordRecoveryVC: PasswordRecoveryViewcontrollerMock, factory: AuthenticationViewControllerFactoryMock, service: LoginServiceMock, presenter: PresenterMock)
    
    private func getSut(factory: AuthenticationViewControllerFactory? = nil,
                        service: AuthenticationProvider) -> AuthenticationHandler {
        let authentication = AuthenticationHandler(viewControllerFactory: factory,
                                                   service: service)
        return authentication
    }
    
    private func getCompleteSut(testPresenter: PresenterMock? = nil,
                                testFactory: AuthenticationViewControllerFactoryMock? = nil,
                                testService: LoginServiceMock? = nil) -> SutResponce {
        let presenter = testPresenter ?? PresenterMock()
        let loginViewController = LoginViewControllerMock()
        let registrationViewController = RegistrationViewControllerMock()
        let passwordRecoveryViewController = PasswordRecoveryViewcontrollerMock()
        let factory = testFactory ?? AuthenticationViewControllerFactoryMock(onLoginViewControllerRequested: {
            return loginViewController
        }, onRegistrationViewControllerRequested: {
            return registrationViewController
        }, onErrorViewControllerRequested: {
            return UIAlertController()
        }, onPasswordRecoveryViewControllerRequested: {
            return passwordRecoveryViewController
        })
        let service = testService ?? LoginServiceMock()
        let sut = getSut(factory: factory, service: service)
        return SutResponce(authHandler: sut,
                           loginVc: loginViewController,
                           registrationVC: registrationViewController,
                           passwordRecoveryVC: passwordRecoveryViewController,
                           factory: factory,
                           service: service,
                           presenter: presenter)
    }
    
    func testAuthenticationHandlerShouldPresentLoginOnPresenter() {
        let presenter = PresenterMock()
        var presented: Bool = false
        presenter.onPresent = { vc in
            presented = true
        }
        let sut = getCompleteSut()
        let block: (Result<LoginResponseMock?, Error>) -> Void = { _ in }
        sut.authHandler.authenticate(on: presenter, completion: block)
        XCTAssertTrue(presented)
    }
    
    func testAuthenticationHandlerShouldCallServiceOnRefreshToken() {
        let sut = getCompleteSut()
       sut.service.result = .success(LoginResponseMock())
        var tokenRefreshDidCall = false
        sut.service.onTokenRefreshRequestDidCall = {
            tokenRefreshDidCall = true
        }
        let block: (Result<LoginResponseMock, Error>) -> Void = { _ in }
        sut.authHandler.refresh(completion: block)
        XCTAssertTrue(tokenRefreshDidCall)
    }
    
    func testAuthenticationRefreshShouldCallCompletionWithFailureResultOnFailure() {
        let sut = getCompleteSut()
        var completionDidCall = false
        let block: (Result<LoginResponseMock, Error>) -> Void = { result in
            completionDidCall = true
            switch result {
            case .success(_):
                XCTFail("Should fail")
            case .failure(_):
                break
            }
        }
        sut.authHandler.refresh(completion: block)
        XCTAssertTrue(completionDidCall)
    }
    
    func testAuthenticationRefreshShouldCallCompletionWithSuccessResultOnSuccess() {
        let sut = getCompleteSut()
        var completionDidCall = false
        sut.service.result = .success(LoginResponseMock())
        let block: (Result<LoginResponseMock, Error>) -> Void = { result in
            completionDidCall = true
            switch result {
            case .success(_):
                break
            case .failure(_):
                XCTFail("Should succeed")
            }
        }
        sut.authHandler.refresh(completion: block)
        XCTAssertTrue(completionDidCall)
    }
}

// Login
extension AuthenticationHandlerTests {
    func testLoginViewControllerShouldBeRequestedToTheFactory() {
        var factoryCalled: Bool = false
        let presenter = PresenterMock()
        let loginViewController = LoginViewControllerMock()
        let registrationViewController = RegistrationViewControllerMock()
        let factory = AuthenticationViewControllerFactoryMock(onLoginViewControllerRequested: {
            factoryCalled = true
            return loginViewController
        }, onRegistrationViewControllerRequested: {
            return registrationViewController
        }, onErrorViewControllerRequested: {
            return UIAlertController()
        }, onPasswordRecoveryViewControllerRequested: {
            return PasswordRecoveryViewcontrollerMock()
        })
        var viewControllerToBePresented: UIViewController?
        presenter.onPresent = { vcToPresent in
            viewControllerToBePresented = vcToPresent
        }
        let block: (Result<LoginResponseMock?, Error>) -> Void = { _ in }
        let sut = getCompleteSut(testPresenter: presenter, testFactory: factory)
        sut.authHandler.authenticate(on: presenter, completion: block)
        XCTAssertTrue(factoryCalled)
        XCTAssertEqual((viewControllerToBePresented as? UINavigationController)?.visibleViewController, loginViewController)
        
    }
    
    func testLoginOnBackEventShoudCallCompletionWithASuccessfullResultAndNoToken() {
        let sut = getCompleteSut()
        var completionCalled = false
        let block: (Result<LoginResponseMock?, Error>) -> Void = { result in
            completionCalled = true
            switch result {
            case .success(let token):
                XCTAssertNil(token)
            case .failure(_):
                XCTFail("Should compelete with success")
            }
        }
        sut.authHandler.authenticate(on: sut.presenter, completion: block)
        sut.loginVc.onBackDidTap?()
        XCTAssertTrue(completionCalled)
    }
    
    func testLoginOnLoginRequestShouldCallLoginService() {
        let service = LoginServiceMock()
        service.result = .success(LoginResponseMock())
        let sut = getCompleteSut(testService: service)
        var serviceCalled = false
        let expectedUser = "user"
        let expectedPassword = "psw"
        service.onLoginRequestDidCall = { user, password in
            serviceCalled = true
            XCTAssertEqual(user, expectedUser)
            XCTAssertEqual(password, expectedPassword)
        }
        let block: (Result<LoginResponseMock?, Error>) -> Void = { _ in }
        sut.authHandler.authenticate(on: sut.presenter, completion: block)
        let email = AuthenticationField(type: .email(text: "Email", textType: .defaultTextField, optional: true), value: expectedUser)
        let password = AuthenticationField(type: .password(text: "Password", textType: .defaultTextField, optional: true), value: expectedPassword)
        sut.loginVc.onFormWillSend?([email, password])
        XCTAssertTrue(serviceCalled)
    }
    
    func testLoginOnLoginSuccessShouldReturnToken() {
        let service = LoginServiceMock()
        service.result = .success(LoginResponseMock())
        let sut = getCompleteSut(testService: service)
        var completionCalled = false
        let block: (Result<LoginResponseMock?, Error>) -> Void = { result in
            completionCalled = true
            switch result {
            case .success(let token):
                XCTAssertNotNil(token)
            case .failure(_):
                XCTFail("Should compelete with success")
            }
        }
        sut.authHandler.authenticate(on: sut.presenter, completion: block)
        let email = AuthenticationField(type: .email(text: "Email", textType: .defaultTextField, optional: true), value: "aaa")
        let password = AuthenticationField(type: .password(text: "Password", textType: .defaultTextField, optional: true), value: "aaa")
        sut.loginVc.onFormWillSend?([email, password])
        XCTAssertTrue(completionCalled)
    }
    
    func testLoginOnLoginWithNetworkErrorShouldReturnResultWithError() {
        let service = LoginServiceMock()
        let sut = getCompleteSut(testService: service)
        var presentedError = false
        sut.loginVc.onPresent = { vc in
            presentedError = true
        }
        let block: (Result<LoginResponseMock?, Error>) -> Void = { _ in }
        sut.authHandler.authenticate(on: sut.presenter, completion: block)
        let email = AuthenticationField(type: .email(text: "Email", textType: .defaultTextField, optional: true), value: "aaa")
        let password = AuthenticationField(type: .password(text: "Password", textType: .defaultTextField, optional: true), value: "aaa")
        sut.loginVc.onFormWillSend?([email, password])
        XCTAssertTrue(presentedError)
    }
    
    func testLoginOnRegisterShouldPresentRegistrationViewController() {
        let sut = getCompleteSut()
        sut.presenter.onPresent = { _ in
            sut.loginVc.onForgotPasswordDidTap?()
        }
        var presentCalled: Bool = false
        sut.loginVc.onPresent = { vc in
            presentCalled = true
        }
        let block: (Result<LoginResponseMock?, Error>) -> Void = { _ in }
        sut.authHandler.authenticate(on: sut.presenter, completion: block)
        XCTAssertTrue(presentCalled)
    }
    
    func testLoginOnPasswordRecoveryFactoryShouldBeCalledAndReturnPasswordRecoveryViewController() {
        let sut = getCompleteSut()
        sut.presenter.onPresent = { _ in
            sut.loginVc.onForgotPasswordDidTap?()
        }
        var passwordRecoveryDidCall = false
        sut.factory.onPasswordRecoveryViewControllerRequested = {
            passwordRecoveryDidCall = true
            return PasswordRecoveryViewcontrollerMock()
        }
        let block: (Result<LoginResponseMock?, Error>) -> Void = { _ in }
        sut.authHandler.authenticate(on: sut.presenter, completion: block)
        XCTAssertTrue(passwordRecoveryDidCall)
    }
}

// Registration
extension AuthenticationHandlerTests {
    func testRegistrationOnBackShouldDismiss() {
        let sut = getCompleteSut()
        sut.presenter.onPresent = { _ in
            sut.loginVc.onForgotPasswordDidTap?()
        }
        var dismissCalled: Bool = false
        sut.loginVc.onPresent = { vc in
            let vc = (vc as? UINavigationController)
            XCTAssertTrue(vc?.visibleViewController is AuthenticationInterfaceEventHandler)
            (vc?.visibleViewController as? AuthenticationInterfaceEventHandler)?.onBackDidTap?()
        }
        sut.passwordRecoveryVC.onDismiss = {
            dismissCalled = true
        }
        let block: (Result<LoginResponseMock?, Error>) -> Void = { _ in }
        sut.authHandler.authenticate(on: sut.presenter, completion: block)
        XCTAssertTrue(dismissCalled)
    }
    
    func testOnRegistrationButtonShouldCallService() {
        let sut = getCompleteSut()
        sut.presenter.onPresent = { _ in
            let email = AuthenticationField(type: .email(text: "Email", textType: .defaultTextField, optional: true), value: "expectedEmail")
            let password = AuthenticationField(type: .password(text: "Password", textType: .defaultTextField, optional: true), value: "expectedPassword")
            sut.registrationVC.onFormWillSend?([email, password])
        }
        var serviceCalled: Bool = false
        sut.service.onRegistrationRequestDidCall = { info in
            serviceCalled = true
        }
        let block: (Result<LoginResponseMock?, Error>) -> Void = { _ in }
        sut.authHandler.register(on: sut.presenter, completion: block)
        XCTAssertTrue(serviceCalled)
    }
    
    func testOnRegistrationNetworkRequestSuccessShouldCallFlowCompletionWithToken() {
        let sut = getCompleteSut()
        sut.service.result = .success(LoginResponseMock())
        sut.presenter.onPresent = { _ in
            let email = AuthenticationField(type: .email(text: "Email", textType: .defaultTextField, optional: true), value: "expectedEmail")
            let password = AuthenticationField(type: .password(text: "Password", textType: .defaultTextField, optional: true), value: "expectedPassword")
            sut.registrationVC.onFormWillSend?([email, password])
        }
        var completionCalled: Bool = false
        let block: (Result<LoginResponseMock?, Error>) -> Void = { result in
            completionCalled = true
            switch result {
            case .success(_):
                break
            case .failure(_):
                XCTFail("Should be success")
            }
        }
        sut.authHandler.register(on: sut.presenter, completion: block)
        XCTAssertTrue(completionCalled)
    }
    
    func testOnRegistrationNetworkRequestFailureShouldPresentPopupWithError() {
        let sut = getCompleteSut()
        sut.presenter.onPresent = { _ in
            let email = AuthenticationField(type: .email(text: "Email", textType: .defaultTextField, optional: true), value: "expectedEmail")
            let password = AuthenticationField(type: .password(text: "Password", textType: .defaultTextField, optional: true), value: "expectedPassword")
            sut.registrationVC.onFormWillSend?([email, password])
        }
        var errorPopupCalled = false
        sut.registrationVC.onPresent = { vc in
            errorPopupCalled = true
            XCTAssertTrue(vc is UIAlertController)
        }
        let block: (Result<LoginResponseMock?, Error>) -> Void = { _ in }
        sut.authHandler.register(on: sut.presenter, completion: block)
        XCTAssertTrue(errorPopupCalled)
    }
}

//// Password Recovery
extension AuthenticationHandlerTests {
    func testPasswordRecoveryShouldBeDismissedOnBackButtonDidTap() {
        let sut = getCompleteSut()
        sut.presenter.onPresent = { _ in
            sut.loginVc.onForgotPasswordDidTap?()
        }
        var backCalled: Bool = false
        sut.loginVc.onPresent = { _ in
            sut.passwordRecoveryVC.onBackDidTap?()
        }
        sut.passwordRecoveryVC.onDismiss = {
            backCalled = true
        }
        let block: (Result<LoginResponseMock?, Error>) -> Void = { _ in }
        sut.authHandler.authenticate(on: sut.presenter, completion: block)
        XCTAssertTrue(backCalled)
    }
    
    func testPasswordRecoveryShouldShowErrorOnNetworkErrorRetrievingPassword() {
        let sut = getCompleteSut()
        sut.presenter.onPresent = { _ in
            sut.loginVc.onForgotPasswordDidTap?()
        }
        var errorPopupCalled: Bool = false
        let expectedPassword = "a"
        sut.loginVc.onPresent = { _ in
            sut.passwordRecoveryVC.onFormWillSend?([AuthenticationField(type: .email(text: "Email", textType: .defaultTextField, optional: true), value: expectedPassword)])
        }
        sut.passwordRecoveryVC.onPresent = { vc in
            errorPopupCalled = true
            XCTAssertTrue(vc is UIAlertController)
        }
        let block: (Result<LoginResponseMock?, Error>) -> Void = { _ in }
        sut.authHandler.authenticate(on: sut.presenter, completion: block)
        XCTAssertTrue(errorPopupCalled)
    }
    
    //    func testPasswordRecoveryShouldDismissOnNetworkSuccess() {
    //        let sut = getCompleteSut()
    //        sut.presenter.onPresent = { _ in
    //            sut.loginVc.onForgotPasswordDidTap?()
    //        }
    //        sut.service.passwordRecoveryResult = .success(true)
    //        var passwordRecoveredCorrectlyDidCall: Bool = false
    //        let expectedPassword = "a"
    //        sut.loginVc.onPresent = { _ in
    //            sut.passwordRecoveryVC.onRecoverPasswordDidRequest?(expectedPassword)
    //        }
    //        sut.passwordRecoveryVC.onPasswordRecoveredCorrectly = {
    //            passwordRecoveredCorrectlyDidCall = true
    //        }
    //        sut.authHandler.authenticate(on: sut.presenter) { result in }
    //        XCTAssertTrue(passwordRecoveredCorrectlyDidCall)
    //    }
    //
    func testAuthenticationStoryboardFinderForLoginShouldReturnAuthenticationStoryboardId() {
        let finder = DefaultAuthenticationStoryboardFinder()
        XCTAssertEqual(try! finder.storyboardId(for: "DefaultCustomizebleAuthenticationViewController"), "Registration")
    }
    
    func testAuthenticationStoryboardForAuthenticationFinderShouldReturnAuthenticationStoryboardId() {
        let finder = DefaultAuthenticationStoryboardFinder()
        XCTAssertEqual(try! finder.storyboardId(for: "DefaultAuthenticationViewController"), "Authentication")
    }
    
    func testAuthenticationStoryboardFinderShouldReturnThreeLoginViewController() {
        let finder = DefaultAuthenticationStoryboardFinder()
        XCTAssertEqual(finder.viewControllers(inStoryboard: "Authentication").count, 2)
    }
    
    func testAuthenticationStoryboardFinderShouldReturnLoginViewControllerAmongViewControllers() {
        let finder = DefaultAuthenticationStoryboardFinder()
        XCTAssertTrue(finder.viewControllers(inStoryboard: "Authentication").contains("DefaultCustomizebleAuthenticationViewController"))
    }
    
    func testRegistrationShouldCallService() {
        let sut = getCompleteSut()
        var registrationDidCall = false
        sut.service.onRegistrationRequestDidCall = { info in
            registrationDidCall = true
            XCTAssertEqual(info.count, 1)
            switch info[0].type {
            case .email(_, _, _):
                break
            default:
                XCTFail("Should be email")
            }
            XCTAssertEqual(info[0].value, "aaaa")
        }
        let block: (Result<LoginResponseMock?, Error>) -> Void = { _ in }
        sut.authHandler.register(on: sut.presenter, completion: block)
        sut.registrationVC.onFormWillSend?([AuthenticationField(type: .email(text: "aaa", textType: .defaultTextField, optional: true), value: "aaaa")])
        XCTAssertTrue(registrationDidCall)
    }
    
    func testRegistrationShouldCallPublicCallback() {
        let sut = getCompleteSut()
        var registrationDidCall = false
        sut.service.result = .success(LoginResponseMock())
        sut.service.onRegistrationRequestDidCall = { info in }
        let block: (Result<LoginResponseMock?, Error>) -> Void = { result in
            registrationDidCall = true
            switch result {
            case .success(_):
                break
            case .failure(_):
                XCTFail("should succeed")
            }
        }
        sut.authHandler.register(on: sut.presenter, completion: block)
        sut.registrationVC.onFormWillSend?([AuthenticationField(type: .email(text: "aaa", textType: .defaultTextField, optional: true), value: "aaaa")])
        XCTAssertTrue(registrationDidCall)
    }
    
    func testLoginOnNetworkRequestActivityShouldStart() {
        let sut = getCompleteSut()
        _ = sut.loginVc.view
        let block: (Result<LoginResponseMock?, Error>) -> Void = { _ in }
        sut.authHandler.authenticate(on: sut.presenter, completion: block)
        let email = AuthenticationField(type: .email(text: "aaa", textType: .defaultTextField, optional: true), value: "aaaa")
        let password = AuthenticationField(type: .password(text: "aaa", textType: .defaultTextField, optional: true), value: "bbb")
        var activityDidStart = false
        sut.loginVc.onActivityDidStart = {
            activityDidStart = true
        }
        sut.loginVc.onFormWillSend?([email, password])
        XCTAssertTrue(activityDidStart)
    }
    
    func testLoginOnNetworkRequestActivityShouldStopWhenCompleted() {
        let sut = getCompleteSut()
        _ = sut.loginVc.view
        let block: (Result<LoginResponseMock?, Error>) -> Void = { _ in }
        sut.authHandler.authenticate(on: sut.presenter, completion: block)
        let email = AuthenticationField(type: .email(text: "aaa", textType: .defaultTextField, optional: true), value: "aaaa")
        let password = AuthenticationField(type: .password(text: "aaa", textType: .defaultTextField, optional: true), value: "bbb")
        var activityDidStart = false
        sut.loginVc.onActivityDidStart = {
            activityDidStart = true
        }
        var activityDidStop = false
        sut.loginVc.onActivityDidStop = {
            XCTAssertTrue(activityDidStart)
            activityDidStop = true
        }
        sut.loginVc.onFormWillSend?([email, password])
        XCTAssertTrue(activityDidStop)
    }
}
