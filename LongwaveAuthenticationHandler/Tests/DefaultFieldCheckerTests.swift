//
//  DefaultFieldCheckerTests.swift
//  AuthenticationHandlerTests
//
//  Created by Alessio Bonu on 10/01/2020.
//  Copyright © 2020 LongWave. All rights reserved.
//

import XCTest
@testable import AuthenticationHandler
class DefaultFieldCheckerTests: XCTestCase {

    func testIfUserNameIsEmptyCheckShouldReturnError() {
        let checker = DefaultFieldChecker()
        switch checker.check(text: "", for: .username) {
        case .consistent:
            XCTFail("should not be consistent")
        case .checkError(let errorStr):
            XCTAssertEqual(errorStr, "Chould contain at least one character")
        }
    }

    func testIfPasswordIsEmptyCheckShouldReturnError() {
        let checker = DefaultFieldChecker()
        switch checker.check(text: "", for: .password) {
        case .consistent:
            XCTFail("should not be consistent")
        case .checkError(let errorStr):
            XCTAssertEqual(errorStr, "Chould contain at least one character")
        }
    }
    
    func testIfUserNameHasAtLeastOneCharacterTheCheckIsConsistent() {
        let checker = DefaultFieldChecker()
        switch checker.check(text: "a", for: .username) {
        case .consistent:
            break
        case .checkError(_):
            XCTFail("should not be consistent")
        }
    }
    
    func testIfPasswordHasAtLeastOneCharacterTheCheckIsConsistent() {
        let checker = DefaultFieldChecker()
        switch checker.check(text: "a", for: .username) {
        case .consistent:
            break
        case .checkError(_):
            XCTFail("should not be consistent")
        }
    }
}
