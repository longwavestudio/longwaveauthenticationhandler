# LongwaveAuthenticationHandler

[![CI Status](https://img.shields.io/travis/alessio.bonu@gmail.com/LongwaveAuthenticationHandler.svg?style=flat)](https://travis-ci.org/alessio.bonu@gmail.com/LongwaveAuthenticationHandler)
[![Version](https://img.shields.io/cocoapods/v/LongwaveAuthenticationHandler.svg?style=flat)](https://cocoapods.org/pods/LongwaveAuthenticationHandler)
[![License](https://img.shields.io/cocoapods/l/LongwaveAuthenticationHandler.svg?style=flat)](https://cocoapods.org/pods/LongwaveAuthenticationHandler)
[![Platform](https://img.shields.io/cocoapods/p/LongwaveAuthenticationHandler.svg?style=flat)](https://cocoapods.org/pods/LongwaveAuthenticationHandler)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

LongwaveAuthenticationHandler is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'LongwaveAuthenticationHandler'
```

## Author

alessio.bonu@gmail.com, alessio.bonu@gmail.com

## License

LongwaveAuthenticationHandler is available under the MIT license. See the LICENSE file for more info.
